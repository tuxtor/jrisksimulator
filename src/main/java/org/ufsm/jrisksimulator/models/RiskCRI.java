/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 * This class represents the composite risk index of each of the evaluated risks.
 * 
 * Depending on the simulation stage, this class can represent a partial or final result. 
 * 
 * Regular Getters and Setters were avoided by using {@link lombok.Lombok}
 *
 * @author tuxtor
 */
@Data
public class RiskCRI implements Comparable<RiskCRI> {

    private int id;
    private double cri;

    private List<RiskEventKRI> riskKriList;
    private double riskPerformance;
    private PerformanceProfile performanceProfile;
    private RiskProfile riskProfile;

    private DescriptiveStatistics riskOpinionsPerAgent = new DescriptiveStatistics();

    public RiskCRI(int id) {
        this.id = id;
        riskKriList = new ArrayList<RiskEventKRI>();
    }

    /**
     * Updates the CRI holded by this object on the fly
     * @param riskvalue 
     */
    public void updateCRI(double riskvalue) {
        riskOpinionsPerAgent.addValue(riskvalue);
        this.cri = riskOpinionsPerAgent.getMean();
    }

    
    @Override
    public int compareTo(RiskCRI t) {
        return Double.compare(t.getCri(), this.getCri());
    }

    @Override
    public String toString() {
        return "RiskCRI{" + "id=" + id + ", cri=" + cri + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RiskCRI other = (RiskCRI) obj;
        return this.id == other.id;
    }

}
