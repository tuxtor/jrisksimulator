/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;

/**
 * This class represents the configuration of a given simulation.
 * 
 * As can be observed all properties are implemented by using JavaFX property equivalents
 * to data types, consequently the usage of JavaFX value bindings is enabled.
 * <br />
 * For backward compatibility wrapper methods with plain properties equivalents 
 * are also implemented and lombok usage is avoided and discouraged :D.
 * 
 * @author tuxtor
 */
public class SimulationConfig {

    private LongProperty agentsQty;
    private LongProperty evaluatorsQty;
    private LongProperty goodOpinionsQty;
    private LongProperty risksQty;
    private LongProperty wrongRisksQty;
    private LongProperty kriQty;
    private BooleanProperty trustEnabled;
    private BooleanProperty bootstrapTrustEnabled;
    private BooleanProperty viewerEnabled;
    private LongProperty iterationsQty;
    private LongProperty faliureRateSamplesQty;
    private DoubleProperty gammaQty;
    private DoubleProperty tethaQty;
    private DoubleProperty bethaQty;
    private LongProperty typeOfEvolution;//0 - positive, 1 - negative, 2 - alternant, 3 - random
    private List<RiskCRI> criticalRisks;
    private List<RiskCRI> secondaryRisks;
    private List<RiskCRI> randomRisks;
    //Trust values
    private DoubleProperty fixedPeerTrust;
    private DoubleProperty fixedPerformance;
    private LongProperty connectionsDensity;
    private BooleanProperty wrongBootstrapTrustEnabled;
    private BooleanProperty fixedPerformanceEnabled;

    public SimulationConfig() {
        setTest2Config();
    }

    /**
     * Basic set of properties for debug purposes
     */
    public void setTest1Config() {
        criticalRisks = new ArrayList<>();
        secondaryRisks = new ArrayList<>();
        randomRisks = new ArrayList<>();
        agentsQty = new SimpleLongProperty(50);
        evaluatorsQty = new SimpleLongProperty(5);
        goodOpinionsQty = new SimpleLongProperty(1);
        risksQty = new SimpleLongProperty(15);
        wrongRisksQty = new SimpleLongProperty(3);
        kriQty = new SimpleLongProperty(4);
        trustEnabled = new SimpleBooleanProperty(false);
        viewerEnabled = new SimpleBooleanProperty(true);
        bootstrapTrustEnabled = new SimpleBooleanProperty(true);
        iterationsQty = new SimpleLongProperty(50);
        faliureRateSamplesQty = new SimpleLongProperty(16);
        gammaQty = new SimpleDoubleProperty(0.6);
        tethaQty = new SimpleDoubleProperty(0.2);
        bethaQty = new SimpleDoubleProperty(0.8);
        typeOfEvolution = new SimpleLongProperty(0);

        fixedPeerTrust = new SimpleDoubleProperty(1);
        fixedPerformance = new SimpleDoubleProperty(0.4);
        wrongBootstrapTrustEnabled = new SimpleBooleanProperty(false);
        fixedPerformanceEnabled = new SimpleBooleanProperty(false);
        connectionsDensity = new SimpleLongProperty(2);

    }

    /**
     * Basic set of properties for debug purposes
     */
    public void setTest2Config() {
        criticalRisks = new ArrayList<>();
        secondaryRisks = new ArrayList<>();
        randomRisks = new ArrayList<>();
        agentsQty = new SimpleLongProperty(50);
        evaluatorsQty = new SimpleLongProperty(10);
        goodOpinionsQty = new SimpleLongProperty(2);
        risksQty = new SimpleLongProperty(15);
        wrongRisksQty = new SimpleLongProperty(3);
        kriQty = new SimpleLongProperty(4);
        trustEnabled = new SimpleBooleanProperty(true);
        viewerEnabled = new SimpleBooleanProperty(true);
        bootstrapTrustEnabled = new SimpleBooleanProperty(true);
        iterationsQty = new SimpleLongProperty(10);
        faliureRateSamplesQty = new SimpleLongProperty(1);
        gammaQty = new SimpleDoubleProperty(0.6);
        tethaQty = new SimpleDoubleProperty(0.2);
        bethaQty = new SimpleDoubleProperty(0.9);
        typeOfEvolution = new SimpleLongProperty(0);

        fixedPeerTrust = new SimpleDoubleProperty(1);
        fixedPerformance = new SimpleDoubleProperty(0.4);
        wrongBootstrapTrustEnabled = new SimpleBooleanProperty(true);
        fixedPerformanceEnabled = new SimpleBooleanProperty(false);
        connectionsDensity = new SimpleLongProperty(2);
    }

    public long getAgentsQty() {
        return agentsQty.get();
    }

    public void setAgentsQty(long agentsQty) {
        this.agentsQty.set(agentsQty);
    }

    public long getEvaluatorsQty() {
        return evaluatorsQty.get();
    }

    public void setEvaluatorsQty(long evaluatorsQty) {
        this.evaluatorsQty.set(evaluatorsQty);
    }

    public long getGoodOpinionsQty() {
        return goodOpinionsQty.get();
    }

    public void setGoodOpinionsQty(long goodOpinionsQty) {
        this.goodOpinionsQty.set(goodOpinionsQty);
    }

    public long getRisksQty() {
        return risksQty.get();
    }

    public void setRisksQty(long risksQty) {
        this.risksQty.set(risksQty);
    }

    public long getWrongRisksQty() {
        return wrongRisksQty.get();
    }

    public void setWrongRisksQty(long wrongRisksQty) {
        this.wrongRisksQty.set(wrongRisksQty);
    }

    public long getKriQty() {
        return kriQty.get();
    }

    public void setKriQty(long kriQty) {
        this.kriQty.set(kriQty);
    }

    public boolean isTrustEnabled() {
        return trustEnabled.get();
    }

    public void setTrustEnabled(boolean trustEnabled) {
        this.trustEnabled.set(trustEnabled);
    }

    public boolean isBootstrapTrustEnabled() {
        return bootstrapTrustEnabled.get();
    }

    public void setBootstrapTrustEnabled(boolean trustEnabled) {
        this.bootstrapTrustEnabled.set(trustEnabled);
    }

    public boolean isViewerEnabled() {
        return viewerEnabled.get();
    }

    public void setViewerEnabled(boolean viewerEnabled) {
        this.viewerEnabled.set(viewerEnabled);
    }

    public long getIterationsQty() {
        return iterationsQty.get();
    }

    public void setIterationsQty(long iterationsQty) {
        this.iterationsQty.set(iterationsQty);
    }

    public long getFaliureRateSamplesQty() {
        return faliureRateSamplesQty.get();
    }

    public void setFaliureRateSamplesQty(long faliureRateSamplesQty) {
        this.faliureRateSamplesQty.set(faliureRateSamplesQty);
    }

    public double getGammaQty() {
        return gammaQty.get();
    }

    public void setGammaQty(double gammaQty) {
        this.gammaQty.set(gammaQty);
    }

    public double getTethaQty() {
        return tethaQty.get();
    }

    public void setTethaQty(double tethaQty) {
        this.tethaQty.set(tethaQty);
    }

    public double getBethaQty() {
        return bethaQty.get();
    }

    public void setBethaQty(double bethaQty) {
        this.bethaQty.set(bethaQty);
    }

    public long getTypeOfEvolution() {
        return typeOfEvolution.get();
    }

    public void setTypeOfEvolution(long typeOfEvolution) {
        this.typeOfEvolution.set(typeOfEvolution);
    }

    public void setTypeOfEvolution(int typeOfEvolution) {
        this.typeOfEvolution.set(typeOfEvolution);
    }

    public LongProperty agentsQtyProperty() {
        return this.agentsQty;
    }

    public LongProperty evaluatorsQtyProperty() {
        return this.evaluatorsQty;
    }

    public LongProperty goodOpinionsQtyProperty() {
        return this.goodOpinionsQty;
    }

    public LongProperty risksQtyProperty() {
        return this.risksQty;
    }

    public LongProperty wrongRisksQtyProperty() {
        return this.wrongRisksQty;
    }

    public LongProperty kriQtyProperty() {
        return this.kriQty;
    }

    public BooleanProperty trustEnabledProperty() {
        return this.trustEnabled;
    }

    public BooleanProperty viewerEnabledProperty() {
        return this.viewerEnabled;
    }

    public LongProperty iterationsQtyProperty() {
        return this.iterationsQty;
    }

    public LongProperty faliureRateSamplesQtyProperty() {
        return this.faliureRateSamplesQty;
    }

    public LongProperty typeOfEvolutionProperty() {
        return this.typeOfEvolution;
    }

    public DoubleProperty gammaQtyProperty() {
        return this.gammaQty;
    }

    public DoubleProperty tethaQtyProperty() {
        return this.tethaQty;
    }

    public DoubleProperty bethaQtyProperty() {
        return this.bethaQty;
    }

    public boolean isRiskCritical(int riskId) {
        for (RiskCRI risk : this.getCriticalRisks()) {
            if (risk.getId() == riskId) {
                return true;
            }
        }
        return false;
    }

    public boolean isRiskSecondary(int riskId) {
        for (RiskCRI risk : this.getSecondaryRisks()) {
            if (risk.getId() == riskId) {
                return true;
            }
        }
        return false;
    }

    public boolean isRiskRandom(int riskId) {
        for (RiskCRI risk : this.getRandomRisks()) {
            if (risk.getId() == riskId) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "SimulationConfig{" + "agentsQty=" + agentsQty + ", riskAvaliatorsQty=" + evaluatorsQty + ", goodOpinionsQty=" + goodOpinionsQty + ", risksQty=" + risksQty + ", wrongRisksQty=" + wrongRisksQty + ", perRiskKRIQty=" + kriQty + ", trustEnabled=" + trustEnabled + ", viewerEnabled=" + viewerEnabled + ", iterationsQty=" + iterationsQty + ", faliureRateSamplesQty=" + faliureRateSamplesQty + ", gammaQty=" + gammaQty + ", tethaQty=" + tethaQty + ", typeOfEvolution=" + typeOfEvolution + '}';
    }

    public List<RiskCRI> getCriticalRisks() {
        return criticalRisks;
    }

    public void setCriticalRisks(List<RiskCRI> criticalRisks) {
        this.criticalRisks = criticalRisks;
    }

    public List<RiskCRI> getSecondaryRisks() {
        return secondaryRisks;
    }

    public void setSecondaryRisks(List<RiskCRI> secondaryRisks) {
        this.secondaryRisks = secondaryRisks;
    }

    public List<RiskCRI> getRandomRisks() {
        return randomRisks;
    }

    public void setRandomRisks(List<RiskCRI> randomRisks) {
        this.randomRisks = randomRisks;
    }

    public BooleanProperty bootstrapTrustProperty() {
        return bootstrapTrustEnabled;
    }
//-------------------------

    public double getFixedPeerTrust() {
        return fixedPeerTrust.get();
    }

    public void setFixedPeerTrust(double fixedPeerTrustValue) {
        this.fixedPeerTrust.set(fixedPeerTrustValue);
    }

    public double getFixedPerformance() {
        return fixedPerformance.get();
    }

    public void setFixedPerformance(double fixedPerformanceValue) {
        this.fixedPerformance.set(fixedPerformanceValue);
    }

    public boolean isWrongBootstrapTrustEnabled() {
        return wrongBootstrapTrustEnabled.get();
    }

    public void setWrongBootstrapTrustEnabled(boolean wrongBootstrapTrustEnabled) {
        this.wrongBootstrapTrustEnabled.set(wrongBootstrapTrustEnabled);
    }

    public boolean isFixedPerformanceEnabled() {
        return fixedPerformanceEnabled.get();
    }

    public void setFixedPerformanceEnabled(boolean fixedPerformanceEnabled) {
        this.fixedPerformanceEnabled.setValue(fixedPerformanceEnabled);
    }

    public DoubleProperty fixedPeerTrustProperty() {
        return fixedPeerTrust;
    }

    public DoubleProperty fixedPerformanceProperty() {
        return fixedPerformance;
    }

    public BooleanProperty wrongBootstrapTrustEnabledProperty() {
        return wrongBootstrapTrustEnabled;
    }

    public BooleanProperty fixedPerformanceEnabledProperty() {
        return fixedPerformanceEnabled;
    }

    public LongProperty connectionsDensityProperty() {
        return connectionsDensity;
    }

    public long getConnectionsDensity() {
        return connectionsDensity.get();
    }

    public void setConnectionsDensity(long connectionsDensity) {
        this.connectionsDensity.set(connectionsDensity);
    }

}
