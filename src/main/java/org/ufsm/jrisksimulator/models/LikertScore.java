/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

/**
 * Enum that represents likert scales and their numeric equivalents.
 * 
 * The likert scales values were implemented following Amaral's dissertation that
 * was also based in prior research. 
 * @author tuxtor
 */
public enum LikertScore {

    VERY_LOW, LOW, MEDIUM, HIGH, VERY_HIGH;

    /**
     * Generates ARIMA probability numeric equivalent for a given Likert value
     * @return numeric equivalent
     */
    public double getArimaProbability() {
        switch (this) {
            case VERY_LOW:
                return 1;
            case LOW:
                return 2.6;
            case MEDIUM:
                return 3.6;
            case HIGH:
                return 5;
            case VERY_HIGH:
                return 5.9;
        }
        return 1;
    }

    /**
     * Generates AURUM numeric equivalent for a given Likert value
     * @return numeric equivalent
     */
    public double getAurumImpact() {
        switch (this) {
            case VERY_LOW:
                return 10;
            case LOW:
                return 10;
            case MEDIUM:
                return 50;
            case HIGH:
                return 100;
            case VERY_HIGH:
                return 100;
        }
        return 1;
    }

    /**
     * Generates AURUM probability equivalent for a given Likert value
     * @return numeric equivalent
     */
    public double getAurumProbability() {
        switch (this) {
            case VERY_LOW:
                return 0.1;
            case LOW:
                return 0.1;
            case MEDIUM:
                return 0.5;
            case HIGH:
                return 1;
            case VERY_HIGH:
                return 1;
        }
        return 1;
    }

    /**
     * Generates ARIMA impact equivalent for a given Likert value
     * @return numeric equivalent
     */
    public double getArimaImpact() {
        switch (this) {
            case VERY_LOW:
                return 1.2;
            case LOW:
                return 1.2;
            case MEDIUM:
                return 2.1;
            case HIGH:
                return 3;
            case VERY_HIGH:
                return 3;
        }
        return 1;
    }

    /**
     * Generates FMEA and ISRAM equivalents for a given Likert value
     * @return numeric equivalent
     */
    public double getFmeaAndIsram() {
        switch (this) {
            case VERY_LOW:
                return 1;
            case LOW:
                return 2;
            case MEDIUM:
                return 3;
            case HIGH:
                return 4;
            case VERY_HIGH:
                return 5;
        }
        return 1;
    }
    
    /**
     * Generates numeric standard equivalent for a given Likert value
     * @return numeric equivalent
     */
    public int getBoundsValue() {
        switch (this) {
            case VERY_LOW:
                return 0;
            case LOW:
                return 1;
            case MEDIUM:
                return 2;
            case HIGH:
                return 3;
            case VERY_HIGH:
                return 4;
        }
        return 1;
    }
}
