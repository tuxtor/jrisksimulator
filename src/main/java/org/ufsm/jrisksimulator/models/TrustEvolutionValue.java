/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import lombok.Data;

/**
 * Class that represent plottable values.
 * 
 * The main objective of this class is to hold values for plotting purposes since
 * the construction of graphs based on entities is mandatory by JavaFX graphs framework.
 * 
 * @author tuxtor
 */

@Data
public class TrustEvolutionValue {
    
    public double seriesId;
    public double plotValue;

    public TrustEvolutionValue(double seriesId, double plotValue) {
        this.seriesId = seriesId;
        this.plotValue = plotValue;
    }
}