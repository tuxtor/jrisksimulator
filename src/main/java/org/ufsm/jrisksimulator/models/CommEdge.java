/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

/**
 * This class represents the links between agents in the social network,
 * 
 * The representation of links is merely mandatory due JUNG restrictions, 
 * however it was implemented aiming weighted links in future research.
 * @author tuxtor
 */
public class CommEdge {

    private static int nextId = 0;
    private int id;

    public CommEdge() {
        id = nextId++;
    }

    @Override
    public String toString() {
        return String.format("%d", id);
    }
}
