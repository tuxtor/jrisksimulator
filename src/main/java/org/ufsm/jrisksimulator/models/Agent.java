/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.util.Precision;

/**
 *  Entity that represents agent's properties.
 *
 * This class is used  to represent an Agent and his properties (relevance score, trust profiles) 
 * and his neighbors and scores (opinions), 
 * <br />
 * Lombok usage is avoided and discouraged due problems with JUNG methods.
 * 
 * @author tuxtor
 */
public class Agent implements Comparable<Agent> {

    private static int nextId = 0;
    private int id;
    private double relevance;
    private TrustProfile profile;
    private List<RiskScore> risksScore;
    private List<PeerScore> peersScore;
    private PerformanceProfile performanceProfile;

    public Agent() {
        //this.relevance = RandGenerator.getRandDouble(true);
        id = nextId++;
        risksScore = new ArrayList<>();
        peersScore = new ArrayList<>();
        performanceProfile = PerformanceProfile.GAIN;
    }

    public Agent(double trustScore) {
        this.relevance = trustScore;
    }

    public double getRelevance() {
        return relevance;
    }

    public void setRelevance(double trustScore) {
        this.relevance = trustScore;
    }

    public List<RiskScore> getRisksScore() {
        return risksScore;
    }

    public void setRisksScore(List<RiskScore> risksScore) {
        this.risksScore = risksScore;
    }

    public List<PeerScore> getPeersScore() {
        return peersScore;
    }

    public void setPeersScore(List<PeerScore> peersScore) {
        this.peersScore = peersScore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static int getNextId() {
        return nextId;
    }

    public static void setNextId(int aNextId) {
        nextId = aNextId;
    }

    public TrustProfile getProfile() {
        return profile;
    }

    public void setProfile(TrustProfile profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return id + "-" + Precision.round(relevance, 4);
    }

    public int compareTo(Agent a) {
        return Double.compare(a.relevance, this.relevance);
    }

    public PerformanceProfile getPerformanceProfile() {
        return performanceProfile;
    }

    public void setPerformanceProfile(PerformanceProfile performanceProfile) {
        this.performanceProfile = performanceProfile;
    }
}
