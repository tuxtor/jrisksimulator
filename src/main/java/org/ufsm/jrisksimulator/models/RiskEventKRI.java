/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import lombok.Data;

/**
 * This class represents the performance of a KRI event.
 * 
 * Depending on the simulation stage, this class can represent a partial or final score. 
 * 
 * Regular Getters and Setters were avoided by using {@link lombok.Lombok}
 * 
 * @author tuxtor
 */
@Data
public class RiskEventKRI {
    private int id;
    private int riskId;
    private double kri;
}
