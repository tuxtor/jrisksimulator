/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import lombok.Data;
import org.apache.commons.math3.stat.StatUtils;

/**
 * This class represents the score attributed to each risks.
 * 
 * Depending on the simulation stage this class can represent a partial or total score.
 * <br />
 * Most importantly as can be observed in {@link #RiskScore} the results for each of the
 * Amaral's estimation methods are calculated on the fly by using Apache Math Utils.
 * 
 * Regular Getters and Setters where implemented by using {@link lombok.Lombok}
 * 
 * @author tuxtor
 */
@Data
public class RiskScore {
    private int id;
    private LikertScore probability;
    private LikertScore detection;
    private LikertScore frequency;
    private LikertScore impact;
    private LikertScore severity;
    //result metodhs
    private double arima;
    private double isram;
    private double aurum;
    private double fmea;
    private double average;

    /**
     * Constructor for tests without trust usage.
     * @param id Risk id
     * @param probability Value for risk probability of appearance
     * @param detection Value for risk detection difficulty
     * @param frequency Value for frequence of appearance
     * @param impact Value for impact in case of risk exploration
     * @param severity Value for severity of risk impact
     */
    public RiskScore(int id, LikertScore probability, LikertScore detection, LikertScore frequency, LikertScore impact, LikertScore severity) {
        this.id = id;
        this.probability = probability;
        this.detection = detection;
        this.frequency = frequency;
        this.impact = impact;
        this.severity = severity;
//        this.arima = ((getArimaImpact(impact) + ((getArimaProbability(probability) - 1) * 0.5)) * 100) / 5;
//        this.isram = (probability * impact) * 100 / 25;
//        this.aurum = (probability * impact) * 100 / 100;
//        this.fmea = (severity * frequency * detection) * 100 / 125;
        this.arima = ((impact.getArimaImpact() + ((probability.getArimaProbability() - 1) * 0.5)) * 100) / 5;
        this.isram = (probability.getFmeaAndIsram() * impact.getFmeaAndIsram()) * 100 / 25;
        this.aurum = (probability.getAurumProbability() * impact.getAurumImpact()) * 100 / 100;
        this.fmea = (severity.getFmeaAndIsram() * frequency.getFmeaAndIsram() * detection.getFmeaAndIsram()) * 100 / 125;
        double[] allMethods = {arima,isram,aurum,fmea};
        this.average = StatUtils.mean(allMethods);
    }
    
        /**
     * Constructor for tests with trust usage.
     * @param id Risk id
     * @param probability Value for risk probability of appearance
     * @param detection Value for risk detection difficulty
     * @param frequency Value for frequence of appearance
     * @param impact Value for impact in case of risk exploration
     * @param severity Value for severity of risk impact
     * @param trust Value for TruskWebRank estimated trust
     */
    public RiskScore(int id, LikertScore probability, LikertScore detection, LikertScore frequency, LikertScore impact, LikertScore severity, double trust) {
        this.id = id;
        this.probability = probability;
        this.detection = detection;
        this.frequency = frequency;
        this.impact = impact;
        this.severity = severity;
//        this.arima = ((getArimaImpact(impact) + ((getArimaProbability(probability) - 1) * 0.5)) * 100) / 5;
//        this.isram = (probability * impact) * 100 / 25;
//        this.aurum = (probability * impact) * 100 / 100;
//        this.fmea = (severity * frequency * detection) * 100 / 125;
        this.arima = ((impact.getArimaImpact() + ((probability.getArimaProbability() - 1) * 0.5)) * 100) * trust / 5;
        this.isram = (probability.getFmeaAndIsram() * impact.getFmeaAndIsram()) * 100 * trust / 25;
        this.aurum = (probability.getAurumProbability() * impact.getAurumImpact()) * 100  * trust / 100;
        this.fmea = (severity.getFmeaAndIsram() * frequency.getFmeaAndIsram() * detection.getFmeaAndIsram()) * 100 * trust / 125;
        double[] allMethods = {arima,isram,aurum,fmea};
        this.average = StatUtils.mean(allMethods);
    }
    
}
