/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.models;

import lombok.Data;

/**
 * Entity that represents peers scores (trust opinions).
 * 
 * This class is used to hold Agent's opinions about each other.
 * 
 * Regular Getters and Setters were avoided by using {@link lombok.Lombok}
 * 
 * @author tuxtor
 */
@Data
public class PeerScore {
    private int peerId;
    private double directTrustScore;
    private double indirectTrustScore;

    public PeerScore(int peerId, double trustScore) {
        this.peerId = peerId;
        this.directTrustScore = trustScore;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.peerId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PeerScore other = (PeerScore) obj;
        if (this.peerId != other.peerId) {
            return false;
        }
        return true;
    }
    
}
