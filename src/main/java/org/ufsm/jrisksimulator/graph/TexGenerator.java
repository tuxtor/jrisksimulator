/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.graph;

import java.util.List;

/**
 * Utility class to generate tables in TeX.
 * 
 * This class generates TeX representation for colum values and labels.
 * @author tuxtor
 */
public class TexGenerator {

    /**
     * Generates a TeX representation in standard output (console) of a given
     * set of values
     * @param columnLabel Label for each table colum
     * @param columnValues Set of values of rows and columns to be included in the table
     * @return 
     */
    public static String generateTable(String[] columnLabel, List<String[]> columnValues) {
        StringBuilder text = new StringBuilder();
        text.append("\\hline\n");
        text.append(columnLabel[0]);
        for (int i = 1; i < columnLabel.length; i++) {
            text.append(" & ").append(columnLabel[i]);
        }
        text.append("\n\\hline\n");

        for (String[] colums : columnValues) {
            text.append(colums[0]);
            for (int i = 1; i < colums.length; i++) {
                text.append(" & ").append(colums[i]);
            }
            text.append("\n");
        }

        text.append("\n \\hline\n");
        
        return text.toString();
    }
}
