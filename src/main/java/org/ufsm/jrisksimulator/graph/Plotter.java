/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.graph;

import java.util.List;
import java.util.Map;
import javafx.scene.chart.XYChart;
import org.ufsm.jrisksimulator.models.RiskCRI;
import org.ufsm.jrisksimulator.models.SimulationConfig;

/**
 * Interface that represents different graphs plotting methods,
 * 
 * As March/2014 the unique full implementation is {@link JavaFXGraphsPlotter}
 * @author tuxtor
 */
public interface Plotter {

    public XYChart plotRisksPerformance(List<List> samples, RiskCRI risk, boolean trustEnabled);

    public XYChart plotFailureRate(List<List> samples, List<RiskCRI> risks, boolean trustEnabled, SimulationConfig config);

    public XYChart plotGlobalTrustVariance(Map<Integer, List<Double>> agentsRelevanceHistory, SimulationConfig config);

    public XYChart plotCoefficientTrustVariance(Map<Double, List<Double>> agentsRelevanceHistory, SimulationConfig config);
}
