/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import org.ufsm.jrisksimulator.models.RiskCRI;
import org.ufsm.jrisksimulator.models.SimulationConfig;

/**
 * This class implements all plotting (graphing) routines using JavaFX.
 * 
 * This implementation uses JavaFX graphing facilities to create graphical representations
 * of simulation results, graphs procedures are divided by objective as following:
 * <br />
 * 1. Risk performance: Plots the quantity of risks prioritized outside the treatment zone.
 * 2. Failure rate.
 * @author tuxtor
 */
public class JavaFXGraphsPlotter implements Plotter {

    @Override
    public XYChart plotRisksPerformance(List<List> samples, RiskCRI risk, boolean trustEnabled) {
        //final NumberAxis xAxis = new NumberAxis(0, samples.size(), 1);
        CategoryAxis xAxis = new CategoryAxis();
        //final NumberAxis yAxis = new NumberAxis(1, 15, 1);
        NumberAxis yAxis = new NumberAxis(0, 15, 1);
        //ScatterChart<Number, Number> riskChart = new ScatterChart<>(xAxis, yAxis);
        BarChart<String, Number> riskChart = new BarChart<>(xAxis, yAxis);
        xAxis.setLabel("Execução do simulador");
        yAxis.setLabel("Prioridade do risco\n(menor é prioritario)");
        riskChart.setTitle("Simulação vs. prioridade");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Risco r" + risk.getId());
        int i = 0;
        for (List<RiskCRI> riskScores : samples) {
            for (RiskCRI riskCri : riskScores) {
                if (riskCri.equals(risk)) {
                    //series1.getData().add(new XYChart.Data(String.valueOf(++i), riskScores.indexOf(riskCri) + 1));
                    series1.getData().add(new XYChart.Data(String.valueOf(++i), riskScores.indexOf(riskCri) + 1));
                }
            }
        }

        riskChart.getData().addAll(series1);
        riskChart.setMinSize(1024, 576);
        riskChart.setBarGap(0.2);
        return riskChart;
    }

    @Override
    public BarChart plotFailureRate(List<List> perSizeTests, List<RiskCRI> risks, boolean trustEnabled, SimulationConfig config) {
        CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis(0, Math.ceil(config.getIterationsQty() * 0.75), 1);
        BarChart<String, Number> riskChart = new BarChart<>(xAxis, yAxis);
        xAxis.setLabel("Execução do simulador");
        yAxis.setLabel("Incidences in fifth executions (less is better)");

        Map<Integer, XYChart.Series<String, Long>> riskSeries = new HashMap<>(perSizeTests.size());

        riskChart.setTitle("Failure rate with different risk committee sizes");

        long evaluatorsQty = config.getEvaluatorsQty();
        //Data analysis
        int sizeSentinel = 0;
        for (List<List> commiteeSizeResults : perSizeTests) {//Iterates all sizes
            Map<Integer, Long> riskIncidents = new HashMap<>();
            //<editor-fold defaultstate="collapsed" desc="Iterates all samples per size">
            for (List<RiskCRI> listOfCri : commiteeSizeResults) {//Iterates all samples per size
                for (RiskCRI risk : listOfCri) {
                    if (config.getCriticalRisks().contains(risk) && listOfCri.indexOf(risk) > listOfCri.size() / 3) {
                        if (riskIncidents.containsKey(risk.getId())) {
                            riskIncidents.put(risk.getId(), riskIncidents.get(risk.getId()) + 1);
                        } else {
                            riskIncidents.put(risk.getId(), (long) 1);
                        }
                    }
                }
            }
//</editor-fold>

            for (Map.Entry<Integer, Long> entry : riskIncidents.entrySet()) {
                if (riskSeries.containsKey(entry.getKey())) {
                    XYChart.Series<String, Long> riskSerie = riskSeries.get(entry.getKey());
                    riskSerie.getData().add(new XYChart.Data<>(String.valueOf(config.getEvaluatorsQty() + sizeSentinel), entry.getValue()));
                    riskSeries.put(entry.getKey(), riskSerie);
                } else {
                    XYChart.Series<String, Long> riskSerie = new XYChart.Series();
                    riskSerie.setName("Risk " + entry.getKey());
                    riskSerie.getData().add(new XYChart.Data<>(String.valueOf(config.getEvaluatorsQty() + sizeSentinel), entry.getValue()));
                    riskSeries.put(entry.getKey(), riskSerie);
                }
            }
            sizeSentinel++;

        }
        for (XYChart.Series seriesEntry : riskSeries.values()) {
            riskChart.getData().addAll(seriesEntry);
        }
        riskChart.setMinSize(1024, 576);
        return riskChart;
    }

    @Override
    public XYChart plotGlobalTrustVariance(Map<Integer, List<Double>> agentsRelevanceHistory, SimulationConfig config) {
        final NumberAxis xAxis = new NumberAxis(0, config.getIterationsQty(), 1);
        final NumberAxis yAxis = new NumberAxis(0, 1, 0.1);
        ResourceBundle labels = ResourceBundle.getBundle("locales.jrisksimulator", Locale.getDefault());
        xAxis.setLabel(labels.getString("simulator_execution"));
        yAxis.setLabel(labels.getString("relevance_value"));
        final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle(labels.getString("relevance_evolution"));

        List<XYChart.Series> agentSeries = new ArrayList<>();

        for (Map.Entry<Integer, List<Double>> entry : agentsRelevanceHistory.entrySet()) {
            XYChart.Series agentSerie = new XYChart.Series();
            agentSerie.setName(labels.getString("agent") + " " + entry.getKey());
            int step = 0;
            for (Double relevanceScore : entry.getValue()) {
                agentSerie.getData().add(new XYChart.Data(step++, relevanceScore));
            }
            agentSeries.add(agentSerie);
        }

        for (XYChart.Series seriesEntry : agentSeries) {
            lineChart.getData().addAll(seriesEntry);
        }
        lineChart.setMinSize(1024, 576);

        /**
         * Set Series color
         */
        for (int i = 0; i < lineChart.getData().size(); i++) {
            for (Node node : lineChart.lookupAll(".series" + i)) {
                node.getStyleClass().remove("default-color" + (i % 8));
                node.getStyleClass().add("default-color" + (i % 10));
            }
        }

        /**
         * Set Legend items color
         */
        int i = 0;
        for (Node node : lineChart.lookupAll(".chart-legend-item")) {
            if (node instanceof Label && ((Label) node).getGraphic() != null) {
                ((Label) node).getGraphic().getStyleClass().remove("default-color" + (i % 8));
                ((Label) node).getGraphic().getStyleClass().add("default-color" + (i % 10));
            }
            i++;
        }

        return lineChart;
    }

    @Override
    public XYChart plotCoefficientTrustVariance(Map<Double, List<Double>> agentsRelevanceHistory, SimulationConfig config) {
        final NumberAxis xAxis = new NumberAxis(0, config.getIterationsQty(), 1);
        final NumberAxis yAxis = new NumberAxis(0, 1, 0.1);
        ResourceBundle labels = ResourceBundle.getBundle("locales.jrisksimulator", Locale.getDefault());
        xAxis.setLabel(labels.getString("simulator_execution"));
        yAxis.setLabel(labels.getString("relevance_value"));
        final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle(labels.getString("relevance_evolution"));

        List<XYChart.Series> agentSeries = new ArrayList<>();

        for (Map.Entry<Double, List<Double>> entry : agentsRelevanceHistory.entrySet()) {
            XYChart.Series agentSerie = new XYChart.Series();
            agentSerie.setName("" + entry.getKey());
            System.out.println(entry.getKey());
            int step = 0;
            for (Double relevanceScore : entry.getValue()) {
                agentSerie.getData().add(new XYChart.Data(step++, relevanceScore));
            }
            agentSeries.add(agentSerie);
        }

        for (XYChart.Series seriesEntry : agentSeries) {
            lineChart.getData().addAll(seriesEntry);
        }
        lineChart.setMinSize(1024, 576);

        /**
         * Set Series color
         */
        for (int i = 0; i < lineChart.getData().size(); i++) {
            for (Node node : lineChart.lookupAll(".series" + i)) {
                node.getStyleClass().remove("default-color" + (i % 8));
                node.getStyleClass().add("default-color" + (i % 10));
            }
        }

        /**
         * Set Legend items color
         */
        int i = 0;
        for (Node node : lineChart.lookupAll(".chart-legend-item")) {
            if (node instanceof Label && ((Label) node).getGraphic() != null) {
                ((Label) node).getGraphic().getStyleClass().remove("default-color" + (i % 8));
                ((Label) node).getGraphic().getStyleClass().add("default-color" + (i % 10));
            }
            i++;
        }

        return lineChart;
    }

}
