/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.graph;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.samples.SimpleGraphDraw;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import org.ufsm.jrisksimulator.models.Agent;
import org.ufsm.jrisksimulator.models.CommEdge;

/**
 * This class creates a graphical representation of a given graph in
 * JUNG's {@link Graph} format.
 * 
 * Note that this class is a singleton implementation since a graph can
 * be generated/displayed once.
 * @author tuxtor
 */
public class GraphPlotter{

    private static SimpleGraphDraw sgd = new SimpleGraphDraw();
    private static Layout<Agent, CommEdge> layout;
    private static BasicVisualizationServer<Agent, CommEdge> vv;

    /**
     * Singleton generator/access method
     * @return singleton GraphPlotter representation
     */
    public static GraphPlotter getInstance() {
        return GraphDisplayHolder.INSTANCE;
    }

    /** 
     * Singleton holder
     */
    private static class GraphDisplayHolder {

        private static final GraphPlotter INSTANCE = new GraphPlotter();
    }

    /**
     * Produces a graph for a given network
     * @param g graph in JUNG's format
     * @return BufferedImage reprepresentation of a given graph for screen and image visualization.
     */
    public BufferedImage plotGraph(Graph g) {
        layout = new FRLayout<>(g);
        layout.setSize(new Dimension(600, 600));
        vv = new BasicVisualizationServer<>(layout);
        vv.setPreferredSize(new Dimension(650, 650));
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Agent>());

        VisualizationImageServer vis = new VisualizationImageServer(vv.getGraphLayout(), new Dimension(650, 650));

        vis.setBackground(Color.WHITE);
//        vis.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<CommEdge>());
//        vis.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<Agent, CommEdge>());
        vis.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Agent>());
        vis.getRenderer().getVertexLabelRenderer()
                .setPosition(Renderer.VertexLabel.Position.CNTR);
        BufferedImage image = (BufferedImage) vis.getImage(
                new Point2D.Double(vv.getGraphLayout().getSize().getWidth() / 2,
                        vv.getGraphLayout().getSize().getHeight() / 2),
                new Dimension(vv.getGraphLayout().getSize()));
        return image;
    }
}
