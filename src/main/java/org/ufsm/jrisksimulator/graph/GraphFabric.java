/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.graph;

import edu.uci.ics.jung.algorithms.generators.random.ErdosRenyiGenerator;
import edu.uci.ics.jung.algorithms.generators.random.KleinbergSmallWorldGenerator;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import org.apache.commons.collections15.functors.InstantiateFactory;
import org.ufsm.jrisksimulator.models.Agent;
import org.ufsm.jrisksimulator.models.CommEdge;

/**
 * This class creates social networks representations by using graphs.
 * 
 * Creations routines were implemented by using jung's ErdosRenyi and Kleinberg variants.
 * @see <a href=http://jung.sourceforge.net/>
 * @author tuxtor
 */
public class GraphFabric {

    protected static InstantiateFactory undirectedGraphFactory = new InstantiateFactory(UndirectedSparseGraph.class);
    protected static InstantiateFactory<Agent> vertexFactory = new InstantiateFactory<>(Agent.class);
    protected static InstantiateFactory<CommEdge> edgeFactory = new InstantiateFactory<>(CommEdge.class);

    /**
     * Default graph generation with 25 vertices
     * @return Network with 25 vertices and a target degree of 2
     */
    public static Graph createRandomGraph() {
        undirectedGraphFactory = new InstantiateFactory(UndirectedSparseGraph.class);
        vertexFactory = new InstantiateFactory<>(Agent.class);
        edgeFactory = new InstantiateFactory<>(CommEdge.class);
        int targetPopulation = 26;
        int targetVertexDegree = 2;
        int targetEdgeCount = targetVertexDegree*targetPopulation;
        //Restarting graph id
        Agent.setNextId(0);
        Graph graph = getRandomGraph(targetPopulation, targetEdgeCount); //getSmallWorldGraph(population, degree * population);

        return graph;
    }

    /**
     * Generates a random graph with a target(approximated) population size.
     * @param targetPopulation Target population size
     * @return Social network with the target(approximated) number of agents.
     */
    public static Graph createRandomGraph(int targetPopulation) {
        undirectedGraphFactory = new InstantiateFactory(UndirectedSparseGraph.class);
        vertexFactory = new InstantiateFactory<>(Agent.class);
        edgeFactory = new InstantiateFactory<>(CommEdge.class);
        int targetVertexDegree = 3;
        int targetEdgeCount = targetVertexDegree*targetPopulation;
        //Restarting graph id
        Agent.setNextId(0);
        Graph graph = getRandomGraph(targetPopulation, targetEdgeCount); //getSmallWorldGraph(population, degree * population);

        return graph;
    }

    /**
     * Generates a random graph with a target(approximated) population size using Erdos & Renyi algorithm.
     * @param targetPopulation Target population size
     * @param targetEdgeCount Target edges for each of the nodes on the network 
     * @return Social network with the target(approximated) number of agents.
     */
    // p = (2 * |E|) / |V|^2
    private static Graph getRandomGraph(int targetPopulationSize, int targetEdgeCount) {
        double connectionProbability = 2.0 * (((float) targetEdgeCount) / Math.pow(targetPopulationSize, 2));
        connectionProbability = (connectionProbability > 1) ? 1 : connectionProbability;
        connectionProbability = (connectionProbability < 0) ? 0 : connectionProbability;

        ErdosRenyiGenerator<Agent, CommEdge> generator = new ErdosRenyiGenerator<>(undirectedGraphFactory, vertexFactory, edgeFactory, targetPopulationSize, connectionProbability);

        return generator.create();
    }

    /**
     * Generates a graph with small world properties and a target(approximated) population size using Kleinberg's algorithm.
     * @param targetPopulationSize Target population size
     * @param targetEdgeCount Target edges for each of the nodes on the network 
     * @return Social network with the target(approximated) number of agents.
     */
    public static Graph getSmallWorldGraph(int targetPopulationSize, int targetEdgeCount) {
        Agent.setNextId(0);//rebooting agents id
        int numberOfLongDistanceConnections = (int) Math.round((targetEdgeCount - targetPopulationSize) / targetPopulationSize);
        numberOfLongDistanceConnections = (numberOfLongDistanceConnections < 1) ? 1 : numberOfLongDistanceConnections;
        int latticeSize = (int) Math.round(Math.sqrt(targetPopulationSize));
        KleinbergSmallWorldGenerator<Agent, CommEdge> generator = new KleinbergSmallWorldGenerator<>(undirectedGraphFactory, vertexFactory, edgeFactory, latticeSize, 2.0);
        generator.setConnectionCount(numberOfLongDistanceConnections);
        return generator.create();
    }
    
    /**
     * Creates a dummy test graph with three nodes
     * @return Social network with three nodes
     */
    public static UndirectedGraph createTestGraph() {
        UndirectedSparseGraph g = new UndirectedSparseGraph<>();
        g.addVertex((Integer) 1);
        g.addVertex((Integer) 2);
        g.addVertex((Integer) 3);
        g.addEdge("Edge-B", 2, 3);
        return g;
    }
}
