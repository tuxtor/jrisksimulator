/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.util;

import org.ufsm.jrisksimulator.models.LikertScore;
import java.lang.reflect.Method;
import java.util.Random;

/**
 * This class isolates random values generation logic.
 * 
 * RandGenerator is in charge of generating all kind of random values, including
 * per object random values, integer random values and Likert random values.
 * <br />
 * As march 2014 this class uses standard JDK PRNG, however it is recommended
 * to implement/plug other PRNG for large scale simulators.
 * @author tuxtor
 */
public class RandGenerator {

    /**
     * Generic method that is able to fill ANY kind of object with random
     * values, specifically double public properties.
     * @param pObject object to be filled
     * @param continuous defines if the value generation will be based on continuous (real) or stepped (integer) values
     * @param downLimit defines the lower limit 
     * @param upLimit defines the upper limit
     * @return 
     */
    public static Object fillRandValues(Object pObject, boolean continuous, double downLimit, double upLimit) {
        Random randomGenerator = new Random();
        Method methods[] = pObject.getClass().getMethods();
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];
            if (method.getName().substring(0, 3).equalsIgnoreCase("get")) {

                String methodNameSet = "";
                if (method.getName().substring(0, 3).equalsIgnoreCase("get")) {
                    methodNameSet = method.getName().replaceFirst("get", "set");
                } else {
                    methodNameSet = methodNameSet.replaceFirst("is", "set");
                }
                try {
                    Method methodSet = pObject.getClass().getMethod(methodNameSet, method.getReturnType());
                    // It is double
                    if (method.getReturnType().equals(java.lang.Double.TYPE) || method.getReturnType().equals(java.lang.Double.class)) {
                        double randValue = 0;
                        if (continuous) {
                            randValue = downLimit + (upLimit - downLimit) * randomGenerator.nextDouble();
                        } else {//stepped-integer
                            randValue = randomGenerator.nextInt((int) (upLimit - downLimit)) + downLimit;
                        }
                        methodSet.invoke(pObject, randValue);
                    }
                } catch (Exception e) {
                }
            }
        }
        return pObject;
    }

    /**
     * Generates a random value between a given interval 
     * @param continuous defines if the value generation will be based on continuous (real) or stepped (integer) values
     * @param limits upper and lower limits joined on a vararg, if no params are passed 
     * a no limit random value will be generated
     * @return random double value between limits
     */
    public static double getRandDouble(boolean continuous, double... limits) {
        double randValue = 0;
        Random randomGenerator = new Random();
        if (limits.length > 0) {
            if (continuous) {
                randValue = limits[0] + (limits[1] - limits[0]) * randomGenerator.nextDouble();
            } else {
                randValue = randomGenerator.nextInt((int) (limits[1] - limits[0])) + limits[0];
            }
        } else {
            if (continuous) {
                randValue = randomGenerator.nextDouble();
            } else {
                randValue = randomGenerator.nextInt();
            }
        }
        return randValue;
    }
    
    /**
     * Generates a random Likert element between a given interval.
     * 
     * @param limits upper and lower limits wjoined on a vararg
     * @return random likert value between limits
     */
    public static LikertScore getRandLikert(LikertScore... limits){
        int downLimit = limits[0].getBoundsValue();
        int upLimit = limits[1].getBoundsValue();
        Random randomGenerator = new Random();
        return LikertScore.values()[downLimit+randomGenerator.nextInt(upLimit+1-downLimit)];
    }
}
