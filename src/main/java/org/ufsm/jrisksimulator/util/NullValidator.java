/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;

/**
 * Util class designed to validate nullity of object's properties.
 * 
 * @author tuxtor
 */
public class NullValidator {

    /**
     * Validates if the properties of a given object contains null values
     * all routines are implemented by using Java Reflection.
     * @param pObject object whose properties will be validated
     * @return validated Object without null properties
     */
    public static Object validateProperties(Object pObject) {
        Object objeto = pObject;
        Method metodos[] = pObject.getClass().getMethods();
        for (Method metodo : metodos) {
            //Si es un metodo get o is lo utilizo con su equivalente set
            if ((metodo.getName().substring(0, 3).equalsIgnoreCase("get") || metodo.getName().substring(0, 2).equalsIgnoreCase("is")) && !metodo.getName().equals("getClass")) {
                String methodNameSet = "";
                if (metodo.getName().substring(0, 3).equalsIgnoreCase("get")) {
                    methodNameSet = metodo.getName().replaceFirst("get", "set");
                } else {
                    methodNameSet = methodNameSet.replaceFirst("is", "set");
                }
                try {
                    Method metodoSet = pObject.getClass().getMethod(methodNameSet, metodo.getReturnType());
                    if (metodoSet != null) {
                        //Datos numericos
                        //Si es byte
                        if (metodo.getReturnType().equals(java.lang.Byte.class)) {
                            Byte valor = (Byte) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, 0);
                            }
                        }
                        //Si es bigDecimal
                        if (metodo.getReturnType().equals(java.math.BigDecimal.class)) {
                            BigDecimal valor = (BigDecimal) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, new BigDecimal(0));
                            }
                        }
                        // Si es Double
                        if (metodo.getReturnType().equals(java.lang.Double.class)) {
                            Double valor = (Double) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, new Double(0));
                            }
                        }
                        //Si es un string
                        if (metodo.getReturnType().equals(java.lang.String.class)) {
                            String valor = (String) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, "");
                            }
                        }
                        //Si es una lista
                        if (metodo.getReturnType().equals(java.util.List.class)) {
                            List objetosList = (List) metodo.invoke(pObject, new Object[0]);
                            for (Object objetoFromList : objetosList) {
                                NullValidator.validateProperties(objetoFromList);
                            }
                        }
                        //Si es date
                        if (metodo.getReturnType().equals(java.util.Date.class)) {
                            Date valor = (Date) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, new Date());
                            }
                        }
                        //Si es primitivo
                        if (metodo.getReturnType().isPrimitive()) {
                            //los primitivos no permiten null
                        }

                        //JavaFX properties
                        if (metodo.getReturnType().equals(LongProperty.class)) {
                            LongProperty valor = (LongProperty) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, new SimpleLongProperty());
                            }
                        }

                        if (metodo.getReturnType().equals(DoubleProperty.class)) {
                            DoubleProperty valor = (DoubleProperty) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, new SimpleDoubleProperty());
                            }
                        }

                        if (metodo.getReturnType().equals(BooleanProperty.class)) {
                            BooleanProperty valor = (BooleanProperty) metodo.invoke(pObject, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObject, new SimpleBooleanProperty());
                            }
                        }

                    }
                } catch (Exception e) {
                }
            }
        }
        return objeto;
    }

}
