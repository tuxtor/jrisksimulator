/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.util;

/**
 * Singleton logger to avoid loggers dispersion.
 * 
 * @author tuxtor
 */
public class LoggerDaemon {
    
    
    /**
     * Returns and if is necessary creates a LoggerDaemon instance
     * @return  LoggerDaemon unique instance
     */
    public static LoggerDaemon getInstance() {
        return LoggerDaemonHolder.INSTANCE;
    }

    /**
     * Holds LoggerDaemon instance through the program lifecycle
     */
    private static class LoggerDaemonHolder {
        private static final LoggerDaemon INSTANCE = new LoggerDaemon();

    }

            
}
