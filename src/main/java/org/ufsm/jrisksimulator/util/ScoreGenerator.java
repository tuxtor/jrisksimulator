/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.util;

import org.ufsm.jrisksimulator.models.TrustProfile;
import org.ufsm.jrisksimulator.models.LikertScore;
import org.ufsm.jrisksimulator.models.PerformanceProfile;
import org.ufsm.jrisksimulator.models.RiskProfile;
import static org.ufsm.jrisksimulator.models.TrustProfile.COMPANION;
import static org.ufsm.jrisksimulator.models.TrustProfile.FRIEND;
import static org.ufsm.jrisksimulator.models.TrustProfile.KNOWN;

/**
 * This class simulates the generation of opinions based on Likert parameters.
 * 
 * ScoreGenerator is in charge to generate Likert values (represented by 
 *
 * @author tuxtor
 */
public class ScoreGenerator {

    private double lowLimit;
    private double upLimit;

    /**
     * Default constructor
     */
    public ScoreGenerator(){
        lowLimit = 0;
        upLimit = 1;        
    }
    
    /**
     * Alternative constructor that allows to configure the max and min
     * numeric values for the opinions
     * @param lowLimit
     * @param upLimit 
     */
    public ScoreGenerator(double lowLimit, double upLimit) {
        this.lowLimit = lowLimit;
        this.upLimit = upLimit;
    }

    /**
     * Generates a fully and uncontrolled random score between class limits
     * @param continuous defines if the value generation will be based on continuous (real) or stepped (integer) values
     * @return random generated value
     */
    public double generateScore(boolean continuous) {
        return RandGenerator.getRandDouble(continuous, lowLimit, upLimit);
    }

    /**
     * Wrapper that returns the approximation of the generated value using a {#LikertScore} object
     * @param downLimit defines the lower limit
     * @param upLimit defines the upper limit
     * @return generated value in a LikertScore equivalent
     */
    public LikertScore generateLikertScore(LikertScore downLimit, LikertScore upLimit) {

        return RandGenerator.getRandLikert(downLimit, upLimit);
    }

    /**
     * Generates an opinion given a determined trust profile
     * @param continuous defines if the value generation will be based on continuous (real) or stepped (integer) values
     * @param profile enum with the trust profile
     * @return generated value based on the trust profile
     */
    public double generateScore(boolean continuous, TrustProfile profile) {
        double step = (upLimit - lowLimit) / 5;
        switch (profile) {
            case KNOWN:
                return RandGenerator.getRandDouble(continuous, 0, 0.5);
            case COMPANION:
                return RandGenerator.getRandDouble(continuous, 0.5, 0.7);
            case FRIEND:
                return RandGenerator.getRandDouble(continuous, 0.7, 0.9);
        }

        return 0.0;
    }

    /**
     * Generates an opinion given a determined performance profile
     * @param continuous defines if the value generation will be based on continuous (real) or stepped (integer) values
     * @param profile enum with the performance profile
     * @return generated value based on the performance profile
     */
    public double generateScore(boolean continuous, PerformanceProfile profile) {

        switch (profile) {
            case GAIN:
                return RandGenerator.getRandDouble(continuous, 0, 0.5);
            case LOSS:
                return RandGenerator.getRandDouble(continuous, -0.5, 0);
        }

        return 0.0;
    }

    /**
     * Generates an opinion given a determined risk profile
     * @param continuous defines if the value generation will be based on continuous (real) or stepped (integer) values
     * @param profile enum with the risk profile
     * @return generated value based on the risk profile
     */
    public double generateScore(boolean continuous, RiskProfile profile) {

        switch (profile) {
            case CRITICAL:
                return RandGenerator.getRandDouble(continuous, 0, 0.5);
            case SECONDARY:
                return RandGenerator.getRandDouble(continuous, -0.5, 0);
            case RANDOM:
                return RandGenerator.getRandDouble(continuous, -0.5, 0);
        }
        return 0.0;
    }
}
