/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.controllers;

import edu.uci.ics.jung.graph.Graph;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Line;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.LongStringConverter;
import javax.imageio.ImageIO;
import org.ufsm.jrisksimulator.calculator.RiskCalculator;
import org.ufsm.jrisksimulator.calculator.WebTrustCalculator;
import org.ufsm.jrisksimulator.graph.GraphPlotter;
import org.ufsm.jrisksimulator.graph.GraphFabric;
import org.ufsm.jrisksimulator.graph.JavaFXGraphsPlotter;
import org.ufsm.jrisksimulator.graph.Plotter;
import org.ufsm.jrisksimulator.graph.TexGenerator;
import org.ufsm.jrisksimulator.models.Agent;
import org.ufsm.jrisksimulator.models.PerformanceProfile;
import org.ufsm.jrisksimulator.models.RiskCRI;
import org.ufsm.jrisksimulator.models.SimulationConfig;
import org.ufsm.jrisksimulator.models.TrustEvolutionValue;
import org.ufsm.jrisksimulator.util.RandGenerator;

/**
 * The MainWindowController class is used to execute diverse SNA+Risk assessment simulations.
 * 
 * More specifically this class implements the controller for the following simulations:
 * <ul>
 * <li> Coefficients of control simulation
 * <li> Impact of trust simulation
 * <li> Social network and committee size grow simulation
 * </ul>
 * 
 * Test variations can be built upon these tests by modifying the simulator parameters and/or the module implementations
 * 
 * @author Víctor Orozco
 */
public class MainWindowController
        implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Java FX Components injection">
    @FXML //  fx:id="agentsText"
    private TextField agentsText; // Value injected by FXMLLoader

    @FXML //  fx:id="bethaText"
    private TextField bethaText; // Value injected by FXMLLoader

    @FXML //  fx:id="bootstrapTrustBox"
    private CheckBox bootstrapTrustBox; // Value injected by FXMLLoader

    @FXML //  fx:id="displayButton"
    private Button displayButton; // Value injected by FXMLLoader

    @FXML //  fx:id="evaluatorsText"
    private TextField evaluatorsText; // Value injected by FXMLLoader

    @FXML //  fx:id="evolutionBox"
    private ChoiceBox<?> evolutionBox; // Value injected by FXMLLoader

    @FXML //  fx:id="evolutionButon"
    private Button evolutionButon; // Value injected by FXMLLoader

    @FXML //  fx:id="exportButton"
    private Button exportButton; // Value injected by FXMLLoader

    @FXML //  fx:id="faliureText"
    private TextField faliureText; // Value injected by FXMLLoader

    @FXML //  fx:id="fixedPeerTrustText"
    private TextField fixedPeerTrustText; // Value injected by FXMLLoader

    @FXML //  fx:id="fixedPerformanceBox"
    private CheckBox fixedPerformanceBox; // Value injected by FXMLLoader

    @FXML //  fx:id="fixedPerformanceText"
    private TextField fixedPerformanceText; // Value injected by FXMLLoader

    @FXML //  fx:id="connectionsDensityText"
    private TextField connectionsDensityText;

    @FXML //  fx:id="gammaText"
    private TextField gammaText; // Value injected by FXMLLoader

    @FXML //  fx:id="goodOpinionsText"
    private TextField goodOpinionsText; // Value injected by FXMLLoader

    @FXML //  fx:id="impactButton"
    private Button impactButton; // Value injected by FXMLLoader

    @FXML //  fx:id="iterationsText"
    private TextField iterationsText; // Value injected by FXMLLoader

    @FXML //  fx:id="kriText"
    private TextField kriText; // Value injected by FXMLLoader

    @FXML //  fx:id="populationButton"
    private Button populationButton; // Value injected by FXMLLoader

    @FXML //  fx:id="risksText"
    private TextField risksText; // Value injected by FXMLLoader

    @FXML //  fx:id="tethaText"
    private TextField tethaText; // Value injected by FXMLLoader

    @FXML //  fx:id="trustCheckBox"
    private CheckBox trustCheckBox; // Value injected by FXMLLoader

    @FXML //  fx:id="plotMarkerButton"
    private Button plotMarkerButton; // Value injected by FXMLLoader

    @FXML //  fx:id="twrCoefficientsButton"
    private Button twrCoefficientsButton; // Value injected by FXMLLoader

    @FXML //  fx:id="betaButton"
    private Button betaButton; // Value injected by FXMLLoader

    @FXML //  fx:id="viewPane"
    private AnchorPane viewPane; // Value injected by FXMLLoader

    @FXML //  fx:id="viewerCheckBox"
    private CheckBox viewerCheckBox; // Value injected by FXMLLoader

    @FXML //  fx:id="wrongBootstrapBox"
    private CheckBox wrongBootstrapBox; // Value injected by FXMLLoader

    @FXML //  fx:id="wrongRisksText"
    private TextField wrongRisksText; // Value injected by FXMLLoader

    @FXML //  fx:id="x1"
    private TitledPane x1; // Value injected by FXMLLoader

    @FXML //  fx:id="x2"
    private TitledPane x2; // Value injected by FXMLLoader

    @FXML //  fx:id="x3"
    private TitledPane x3; // Value injected by FXMLLoader

    @FXML //  fx:id="grid1"
    private GridPane grid1; // Value injected by FXMLLoader

    @FXML //  fx:id="grid2"
    private GridPane grid2; // Value injected by FXMLLoader

    @FXML //  fx:id="grid3"
    private GridPane grid3; // Value injected by FXMLLoader
    //</editor-fold>
    
    SimulationConfig config;

    /**
     * Method invoked by FXMLLoader when initialization is complete.
     * 
     * @param fxmlFileLocation location of JavaFX xml file
     * @param resources resource bundle representation
     */
    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        assert agentsText != null : "fx:id=\"agentsText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert bethaText != null : "fx:id=\"bethaText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert bootstrapTrustBox != null : "fx:id=\"bootstrapTrustBox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert displayButton != null : "fx:id=\"displayButton\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert evaluatorsText != null : "fx:id=\"evaluatorsText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert evolutionBox != null : "fx:id=\"evolutionBox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert evolutionButon != null : "fx:id=\"evolutionButon\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert exportButton != null : "fx:id=\"exportButton\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert faliureText != null : "fx:id=\"faliureText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert fixedPeerTrustText != null : "fx:id=\"fixedPeerTrustText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert fixedPerformanceBox != null : "fx:id=\"fixedPerformanceBox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert fixedPerformanceText != null : "fx:id=\"fixedPerformanceText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert gammaText != null : "fx:id=\"gammaText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert goodOpinionsText != null : "fx:id=\"goodOpinionsText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert impactButton != null : "fx:id=\"impactButton\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert iterationsText != null : "fx:id=\"iterationsText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert kriText != null : "fx:id=\"kriText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert populationButton != null : "fx:id=\"populationButton\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert risksText != null : "fx:id=\"risksText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert tethaText != null : "fx:id=\"tethaText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert trustCheckBox != null : "fx:id=\"trustCheckBox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert viewPane != null : "fx:id=\"viewPane\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert viewerCheckBox != null : "fx:id=\"viewerCheckBox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert wrongBootstrapBox != null : "fx:id=\"wrongBootstrapBox\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert connectionsDensityText != null : "fx:id=\"connectionsDensityText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert wrongRisksText != null : "fx:id=\"wrongRisksText\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert x1 != null : "fx:id=\"x1\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert x2 != null : "fx:id=\"x2\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert x3 != null : "fx:id=\"x3\" was not injected: check your FXML file 'MainWindow.fxml'.";

        // intialization
        config = new SimulationConfig();
        this.evolutionBox.getSelectionModel().select(0);
        //JavaFX value bindings, using JavaFX properties these binding avoid the creation of change listeners
        StringConverter<? extends Number> longConverter = new LongStringConverter();
        StringConverter<? extends Number> doubleConverter = new DoubleStringConverter();
        this.agentsText.textProperty().bindBidirectional(config.agentsQtyProperty(), (StringConverter<Number>) longConverter);
        this.evaluatorsText.textProperty().bindBidirectional(config.evaluatorsQtyProperty(), (StringConverter<Number>) longConverter);
        this.goodOpinionsText.textProperty().bindBidirectional(config.goodOpinionsQtyProperty(), (StringConverter<Number>) longConverter);
        this.wrongRisksText.textProperty().bindBidirectional(config.wrongRisksQtyProperty(), (StringConverter<Number>) longConverter);
        this.risksText.textProperty().bindBidirectional(config.risksQtyProperty(), (StringConverter<Number>) longConverter);
        this.kriText.textProperty().bindBidirectional(config.kriQtyProperty(), (StringConverter<Number>) longConverter);
        this.iterationsText.textProperty().bindBidirectional(config.iterationsQtyProperty(), (StringConverter<Number>) longConverter);
        this.faliureText.textProperty().bindBidirectional(config.faliureRateSamplesQtyProperty(), (StringConverter<Number>) longConverter);
        this.evolutionBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                config.setTypeOfEvolution((int) t1);
                System.out.println(t1);
            }
        });
        this.gammaText.textProperty().bindBidirectional(config.gammaQtyProperty(), (StringConverter<Number>) doubleConverter);
        this.tethaText.textProperty().bindBidirectional(config.tethaQtyProperty(), (StringConverter<Number>) doubleConverter);
        this.bethaText.textProperty().bindBidirectional(config.bethaQtyProperty(), (StringConverter<Number>) doubleConverter);
        this.fixedPerformanceText.textProperty().bindBidirectional(config.fixedPerformanceProperty(), (StringConverter<Number>) doubleConverter);
        this.fixedPeerTrustText.textProperty().bindBidirectional(config.fixedPeerTrustProperty(), (StringConverter<Number>) doubleConverter);
        this.viewerCheckBox.selectedProperty().bindBidirectional(config.viewerEnabledProperty());
        this.trustCheckBox.selectedProperty().bindBidirectional(config.trustEnabledProperty());
        this.bootstrapTrustBox.selectedProperty().bindBidirectional(config.bootstrapTrustProperty());
        this.wrongBootstrapBox.selectedProperty().bindBidirectional(config.wrongBootstrapTrustEnabledProperty());
        this.fixedPerformanceBox.selectedProperty().bindBidirectional(config.fixedPerformanceEnabledProperty());
        this.connectionsDensityText.textProperty().bindBidirectional(config.connectionsDensityProperty(), (StringConverter<Number>) longConverter);
    }

    //<editor-fold defaultstate="collapsed" desc="Button handlers">
    
    /**
     * Handles the execution of Display Graph button
     * @param event 
     */
    @FXML
    private void handleDisplayGraphAction(ActionEvent event) {
        Agent.setNextId(0);
        Graph network = GraphFabric.getSmallWorldGraph((int) this.config.getAgentsQty(), (int) config.getConnectionsDensity());
        plotGraph(network);
    }

    /**
     * Handles the execution of Impact of Trust button
     * @param event 
     */
    @FXML
    private void handleImpactOfTrustAction(ActionEvent event) {
        simulateRiskQuantfication();
    }

    /**
     * Handles the execution of Evolution button
     * @param event 
     */
    @FXML
    private void handleEvolutionAction(ActionEvent event) {
        simulateEvolutionOfRelevance();
    }

    /**
     * Handles the execution of Population size button
     * @param event 
     */
    @FXML
    private void handlePopulationSizeAction(ActionEvent event) {
        simulateRiskQuantficationWithSteppedComitee();
        //config = new SimulationConfig();
    }

    /**
     * Exports the actual graph in the simulator to /tmp
     * 
     * As 01/04/14 this code is operating system dependant
     * @param event 
     */
    @FXML
    private void handleExportAction(ActionEvent event) {
        exportImage();
    }

    /**
     * Handles the execution of TrustWebRank coefficient button
     * @param event 
     */
    @FXML
    public void handleTwrCoefficientsAction(ActionEvent event) {
        simulateTwrEvolution();
    }
    
    /**
     * Handles the execution of betha coefficient button
     * @param event 
     */
    @FXML
    private void handleBetaAction(ActionEvent event) {
        simulateBetaEvolution();
    }

    /**
     * Handles the execution of plot marker action
     * @param event 
     */
    @FXML
    private void handlePlotMarkerAction(ActionEvent event) {
        Line valueMarker = new Line();
        XYChart chart = (XYChart) viewPane.getChildren().get(0);
        Node chartArea = chart.lookup(".chart-plot-background");
        Bounds chartAreaBounds = chartArea.localToScene(chartArea.getBoundsInLocal());
        // remember scene position of chart area
        double yShift = chartAreaBounds.getMinY();
        // set x parameters of the valueMarker to chart area bounds
        valueMarker.setStartX(70);
        valueMarker.setEndX(chart.getXAxis().getWidth() + 85);

        double displayPosition = chart.getYAxis().getDisplayPosition(5);
        valueMarker.setStartY(yShift + displayPosition);
        valueMarker.setEndY(yShift + displayPosition);
        viewPane.getChildren().add(valueMarker);
    }
    //</editor-fold>

    /**
     * Exports viewer's content to an external file.
     * 
     * This method uses SwingFXUtils to plot the image that is active on the viewer 
     * to a file located in /tmp (tested on Linux and MacOS).
     * 
     * As March/2014 this procedure works only on Linux.
     */
    private void exportImage() {
        Node imageNode = viewPane;
        if (imageNode != null) {
            try {
                WritableImage image = imageNode.snapshot(null, null);
                File file = new File("/tmp/plot.png");
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(image, null);
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException iOException) {
            }
        }
    }

    /**
     * Plots the graph that represents the generated social network.
     * @param g Graph provided by {@link GraphFabric}
     */
    private void plotGraph(Graph g) {
        GraphPlotter plotter = GraphPlotter.getInstance();
        BufferedImage image = plotter.plotGraph(g);

        WritableImage wr = null;
        if (image != null) {
            wr = new WritableImage(image.getWidth(), image.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    pw.setArgb(x, y, image.getRGB(x, y));
                }
            }
        }
        ImageView iv1 = new ImageView();
        iv1.setImage(wr);
        viewPane.getChildren().clear();
        viewPane.getChildren().add(iv1);

    }

    /**
     * Simulates and quantifies the occurrences in no-treatment zone.
     * 
     * simulateRiskQuantfication()
     */
    public void simulateRiskQuantfication() {
        //Simulation entities
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        RiskCalculator riskCalculator = RiskCalculator.getInstance();
        Plotter plotter = new JavaFXGraphsPlotter();

        //Holds all CRI scores per risk
        List<RiskCRI> globalRiskScores;
        //Holds samples of simulation (iteration executions)
        List<List> samples = new ArrayList<List>();
        int iterations = (int) config.getIterationsQty();
        //Simulations
        for (int i = 0; i < iterations; i++) {
            //Graph creation
            Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());
            //Generate and quantfy web Trust
            network = webTrustCalculator.quantifyControlledRelevance(network, config);
            //Generate and quantify risk scores
            globalRiskScores = riskCalculator.quantifyRisks(network, config);
            //Simulate riskPerformace quantification
            //globalRiskScores = performanceCalculator.quantifyPerformance(globalRiskScores, perRiskKRIQty);
            //Sort Risks by priority
            Collections.sort(globalRiskScores);
            samples.add(globalRiskScores);
        }
        if (config.isViewerEnabled()) {
            XYChart chart = plotter.plotRisksPerformance(samples, new RiskCRI(2), config.isTrustEnabled());
            chart.setPrefWidth(viewPane.getWidth());
            chart.setPrefHeight(viewPane.getWidth() / 16 * 9);
            viewPane.getChildren().clear();

            viewPane.getChildren().add(chart);
            viewPane.requestLayout();
        }
        handlePlotMarkerAction(null);
    }

    public void simulateRiskQuantficationWithSteppedComitee() {

        //Creation of simulation elements
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        RiskCalculator riskCalculator = RiskCalculator.getInstance();
        Plotter plotter = new JavaFXGraphsPlotter();
        //Holds a list of tests for every size of commitee members
        List<List> perSizeTests = new ArrayList<>();

        int iterations = (int) config.getIterationsQty();
        //How many different sizes will be tested starting from config.getEvaluatorsQty()
        int failureRateSamples = (int) config.getFaliureRateSamplesQty();
        int bumpSize = 1;
        double proportionFactor = 0.75;
        //Setting critical risks
        config.getCriticalRisks().add(new RiskCRI(1));
        config.getCriticalRisks().add(new RiskCRI(2));
        config.getCriticalRisks().add(new RiskCRI(3));
        double proportionSentinel = config.getGoodOpinionsQty();
        for (int j = 0; j < failureRateSamples; j++) {//Executes test for every size of commitee
            config.setGoodOpinionsQty((long) (config.getEvaluatorsQty() * proportionFactor));
            List<List> iterationsOfSize = new ArrayList<>(iterations); //Creates the iterations of every test, holds a list of 
            for (int i = 0; i < iterations; i++) {

                //Base network structure
                Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());

                //Generate and quantfy web Trust
                network = webTrustCalculator.quantifyControlledRelevance(network, config);
                //List of risk scores per iteration
                List<RiskCRI> globalRiskScores = riskCalculator.quantifyRisks(network, config);

                //Simulate riskPerformace quantification
                //globalRiskScores = performanceCalculator.quantifyPerformance(globalRiskScores, perRiskKRIQty);
                //Sort Risks by priority
                Collections.sort(globalRiskScores);
                iterationsOfSize.add(globalRiskScores);
            }
            config.setEvaluatorsQty(config.getEvaluatorsQty() + 1);

            /*if (config.getEvaluatorsQty() % 5 == 0) {
             config.setGoodOpinionsQty(config.getGoodOpinionsQty() + bumpSize);
             proportionSentinel++;
             }*/
            perSizeTests.add(new ArrayList(iterationsOfSize));
            iterationsOfSize = new ArrayList<>();
        }

        String[] tableHeader = {"Committee Size", "Faliures"};
        List<String[]> tableLines = new ArrayList<>();
        //Resetting evaluators quantity
        config.setEvaluatorsQty(config.getEvaluatorsQty() - failureRateSamples);
        config.setGoodOpinionsQty((long) proportionSentinel);
        System.out.println("Good opinions" + config.getGoodOpinionsQty());
        long evaluatorsQty = config.getEvaluatorsQty();
        long goodOpinionsQty = config.getGoodOpinionsQty();
        //Data analysis
        int totalIncidents = 0;
        for (List<List> commiteeSizeResults : perSizeTests) {//Iterates all sizes
            int incidents = 0;
            for (List<RiskCRI> listOfCri : commiteeSizeResults) {//Iterates all samples per size
                for (RiskCRI risk : listOfCri) {
                    if (config.getCriticalRisks().contains(risk) && listOfCri.indexOf(risk) > listOfCri.size() / 3) {
                        incidents++;
                    }
                }
            }
            tableLines.add(new String[]{(long) (evaluatorsQty * proportionFactor) + "/" + evaluatorsQty,
                String.valueOf(incidents)//,
            //    String.valueOf(incidents * 1000)
            });
            //System.out.println("Evaluators: " + evaluatorsQty + " Incidents: " + incidents);
            evaluatorsQty++;
            if (evaluatorsQty % 5 == 0) {
                goodOpinionsQty += bumpSize;
            }

            totalIncidents += incidents;
        }
        System.out.println(TexGenerator.generateTable(tableHeader, tableLines));
        System.out.println(totalIncidents);
        if (config.isViewerEnabled()) {
            XYChart chart = plotter.plotFailureRate(perSizeTests, config.getCriticalRisks(), config.isTrustEnabled(), config);
            chart.setPrefWidth(viewPane.getWidth());
            chart.setPrefHeight(viewPane.getWidth() / 16 * 9);

            viewPane.getChildren().clear();
            viewPane.getChildren().add(chart);

        }
    }

    /**
     * 
     * @deprecated
     */
    @Deprecated
    public void simulateRiskQuantficationWithVariableComitee() {

        //Creation of simulation elements
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        RiskCalculator riskCalculator = RiskCalculator.getInstance();
        Plotter plotter = new JavaFXGraphsPlotter();
        //Holds a list of tests for every size of commitee members
        List<List> perSizeTests = new ArrayList<>();

        int iterations = (int) config.getIterationsQty();
        //How many different sizes will be tested starting from config.getEvaluatorsQty()
        int failureRateSamples = (int) config.getFaliureRateSamplesQty();
        //Setting critical risks
        config.getCriticalRisks().add(new RiskCRI(1));
        config.getCriticalRisks().add(new RiskCRI(2));
        config.getCriticalRisks().add(new RiskCRI(3));
        int proportionSentinel = 0;
        for (int j = 0; j < failureRateSamples; j++) {//Executes test for every size of commitee
            List<List> iterationsOfSize = new ArrayList<>(iterations); //Creates the iterations of every test, holds a list of 
            for (int i = 0; i < iterations; i++) {

                //Base network structure
                Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());

                //Generate and quantfy web Trust
                network = webTrustCalculator.quantifyControlledRelevance(network, config);
                //List of risk scores per iteration
                List<RiskCRI> globalRiskScores = riskCalculator.quantifyRisks(network, config);

                //Simulate riskPerformace quantification
                //globalRiskScores = performanceCalculator.quantifyPerformance(globalRiskScores, perRiskKRIQty);
                //Sort Risks by priority
                Collections.sort(globalRiskScores);
                iterationsOfSize.add(globalRiskScores);
            }
            config.setEvaluatorsQty(config.getEvaluatorsQty() + 1);

            if (config.getEvaluatorsQty() % 5 == 0) {
                config.setGoodOpinionsQty(config.getGoodOpinionsQty() + 2);
                proportionSentinel++;
            }

            perSizeTests.add(new ArrayList(iterationsOfSize));
            iterationsOfSize = new ArrayList<>();
        }

        String[] tableHeader = {"Committee Size", "Faliures", "Est. Cost"};
        List<String[]> tableLines = new ArrayList<>();
        //Resetting evaluators quantity
        config.setEvaluatorsQty(config.getEvaluatorsQty() - failureRateSamples);
        config.setGoodOpinionsQty(config.getGoodOpinionsQty() - proportionSentinel * 2);
        long evaluatorsQty = config.getEvaluatorsQty();
        //Data analysis
        int totalIncidents = 0;
        for (List<List> commiteeSizeResults : perSizeTests) {//Iterates all sizes
            int incidents = 0;
            for (List<RiskCRI> listOfCri : commiteeSizeResults) {//Iterates all samples per size
                for (RiskCRI risk : listOfCri) {
                    if (config.getCriticalRisks().contains(risk) && listOfCri.indexOf(risk) > listOfCri.size() / 3) {
                        incidents++;
                    }
                }
            }
            tableLines.add(new String[]{String.valueOf(evaluatorsQty),
                String.valueOf(incidents),
                String.valueOf(incidents * 1000)});

            //System.out.println("Evaluators: " + evaluatorsQty + " Incidents: " + incidents);
            evaluatorsQty++;
            totalIncidents += incidents;
        }
        System.out.println(TexGenerator.generateTable(tableHeader, tableLines));
        System.out.println(totalIncidents);
        if (config.isViewerEnabled()) {
            XYChart chart = plotter.plotFailureRate(perSizeTests, config.getCriticalRisks(), config.isTrustEnabled(), config);
            chart.setPrefWidth(viewPane.getWidth());
            chart.setPrefHeight(viewPane.getWidth() / 16 * 9);

            viewPane.getChildren().clear();
            viewPane.getChildren().add(chart);

        }
    }

    /**
     * Implements the simulation of evolution of Relevance over time TODO
     * implement the evolution properly, at this time is a enforced performance
     */
    public void simulateEvolutionOfRelevance() {
        //Simulation entities
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        webTrustCalculator.setSimulationConfig(config);
        Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());
        Plotter plotter = new JavaFXGraphsPlotter();

        //network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        List<Agent> sortedVertices = new ArrayList<>(network.getVertices());
        Collections.sort(sortedVertices);

        //Risk assessment committee
        List<Agent> riskCommittee = sortedVertices.subList(0, (int) config.getEvaluatorsQty());
        //HOOK - Configuring agents desired evolucion

        Iterator riskCommitteeIterator = riskCommittee.iterator();
        int goodOpinions = 0;

        //Setting performance profile
        while (riskCommitteeIterator.hasNext()) {
            Agent evaluator = (Agent) riskCommitteeIterator.next();

            if (goodOpinions < config.getGoodOpinionsQty()) {
                if (config.isWrongBootstrapTrustEnabled()) {
                    evaluator.setPerformanceProfile(PerformanceProfile.LOSS);
                } else {
                    evaluator.setPerformanceProfile(PerformanceProfile.GAIN);
                }
            } else {
                if (config.isWrongBootstrapTrustEnabled()) {
                    evaluator.setPerformanceProfile(PerformanceProfile.GAIN);
                } else {
                    evaluator.setPerformanceProfile(PerformanceProfile.LOSS);
                }
            }
            goodOpinions++;
        }

        Map<Integer, List<Double>> agentsRelevanceHistory = new HashMap<>();
        for (int i = 0; i <= config.getIterationsQty(); i++) {
            //Save Relevance history
            Iterator vertexIterator = network.getVertices().iterator();
            while (vertexIterator.hasNext()) {
                Agent vertex = (Agent) vertexIterator.next();
                if (riskCommittee.contains(vertex)) {
                    List<Double> vertexRelevanceHistory;
                    if (agentsRelevanceHistory.containsKey(vertex.getId())) {
                        vertexRelevanceHistory = agentsRelevanceHistory.get(vertex.getId());
                    } else {
                        vertexRelevanceHistory = new ArrayList<>();
                    }
                    vertexRelevanceHistory.add(vertex.getRelevance());
                    agentsRelevanceHistory.put(vertex.getId(), vertexRelevanceHistory);
                }
            }
            webTrustCalculator.updateTrust(network, riskCommittee, config);
        }
        if (config.isViewerEnabled()) {
            XYChart chart = plotter.plotGlobalTrustVariance(agentsRelevanceHistory, config);
            chart.setPrefWidth(viewPane.getWidth());
            chart.setPrefHeight(viewPane.getWidth() / 16 * 9);
            viewPane.getChildren().clear();
            viewPane.getChildren().add(chart);
        }
    }

    public void simulateTwrEvolution() {
        //Simulation entities
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        webTrustCalculator.setSimulationConfig(config);
        Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());
        Plotter plotter = new JavaFXGraphsPlotter();

        //network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        List<Agent> sortedVertices = new ArrayList<>(network.getVertices());
        Collections.sort(sortedVertices);

        //Risk assessment committee
        List<Agent> riskCommittee = sortedVertices.subList(0, (int) config.getEvaluatorsQty());
        //HOOK - Configuring agents desired evolucion

        int analizedId = riskCommittee.get(0).getId();

        //Simulations
        List<List> valueSeries = new ArrayList<List>();
        List<TrustEvolutionValue> coefficientSerie = new ArrayList<>();

        Map<Double, List<Double>> agentsRelevanceHistory = new TreeMap<>();

        for (int i = 1; i <= 10; i++) {
            double coefficientValue = (double) i / (double) 10;
            if (config.getTypeOfEvolution() == 0) {
                config.setGammaQty(coefficientValue);
            } else {
                config.setTethaQty(coefficientValue);
            }
            for (int j = 0; j <= config.getIterationsQty(); j++) {
                //Save Relevance history
                Iterator vertexIterator = network.getVertices().iterator();
                while (vertexIterator.hasNext()) {
                    Agent vertex = (Agent) vertexIterator.next();
                    if (vertex.getId() == analizedId) {
                        List<Double> vertexRelevanceHistory;
                        if (agentsRelevanceHistory.containsKey(coefficientValue)) {
                            vertexRelevanceHistory = agentsRelevanceHistory.get(coefficientValue);
                        } else {
                            vertexRelevanceHistory = new ArrayList<>();
                        }
                        vertexRelevanceHistory.add(vertex.getRelevance());
                        agentsRelevanceHistory.put(coefficientValue, vertexRelevanceHistory);
                    }
                }
                webTrustCalculator.updateTrustForCoeficients(network, riskCommittee, config);
            }
            //Regenerate trust 
            network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        }

        if (config.isViewerEnabled()) {
            XYChart chart = plotter.plotCoefficientTrustVariance(agentsRelevanceHistory, config);
            chart.setPrefWidth(viewPane.getWidth());
            chart.setPrefHeight(viewPane.getWidth() / 16 * 9);
            viewPane.getChildren().clear();
            viewPane.getChildren().add(chart);
        }
    }

    public void simulateBetaEvolution() {
        //Simulation entities
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        webTrustCalculator.setSimulationConfig(config);
        Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());
        Plotter plotter = new JavaFXGraphsPlotter();

        //network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
        List<Agent> sortedVertices = new ArrayList<>(network.getVertices());
        Collections.sort(sortedVertices);

        //Risk assessment committee
        List<Agent> riskCommittee = sortedVertices.subList(0, (int) config.getEvaluatorsQty());
        //HOOK - Configuring agents desired evolucion

        int analizedId = riskCommittee.get(0).getId();

        //Simulations
        List<List> valueSeries = new ArrayList<List>();
        List<TrustEvolutionValue> coefficientSerie = new ArrayList<>();

        Map<Double, List<Double>> agentsRelevanceHistory = new TreeMap<>();
        double betaSentinel = config.getBethaQty();
        for (int i = 1; i <= 9; i++) {
            double coefficientValue = (double) i / (double) 10;
            config.setBethaQty(coefficientValue);
            try {

                for (int j = 0; j <= config.getIterationsQty(); j++) {
                    //Save Relevance history
                    Iterator vertexIterator = network.getVertices().iterator();
                    while (vertexIterator.hasNext()) {
                        Agent vertex = (Agent) vertexIterator.next();
                        if (vertex.getId() == analizedId) {
                            List<Double> vertexRelevanceHistory;
                            if (agentsRelevanceHistory.containsKey(coefficientValue)) {
                                vertexRelevanceHistory = agentsRelevanceHistory.get(coefficientValue);
                            } else {
                                vertexRelevanceHistory = new ArrayList<>();
                            }
                            vertexRelevanceHistory.add(vertex.getRelevance());
                            agentsRelevanceHistory.put(coefficientValue, vertexRelevanceHistory);
                        }
                    }
                    webTrustCalculator.updateTrustForCoeficients(network, riskCommittee, config);
                }

                //Regenerate trust 
                network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);

            } catch (Exception e) {
                System.out.println(e);
            }
        }
        config.setBethaQty(betaSentinel);
        if (config.isViewerEnabled()) {
            XYChart chart = plotter.plotCoefficientTrustVariance(agentsRelevanceHistory, config);
            chart.setPrefWidth(viewPane.getWidth());
            chart.setPrefHeight(viewPane.getWidth() / 16 * 9);
            viewPane.getChildren().clear();
            viewPane.getChildren().add(chart);
        }
    }

    /**
     * General simulation method.
     * 
     * As march 2014 it has been splitted on different tests and should not be used, it is conserved
     * just for reference purposes.
     * @deprecated replaced by {#simulateEvolutionOfRelevance} and {#simulateRiskQuantification}
     */
    @Deprecated
    public void simulateEvolution() {
        //Simulation entities
        WebTrustCalculator webTrustCalculator = WebTrustCalculator.getInstance();
        Graph network = GraphFabric.getSmallWorldGraph((int) config.getAgentsQty(), (int) config.getConnectionsDensity());
        Plotter plotter = new JavaFXGraphsPlotter();

        webTrustCalculator.setSimulationConfig(config);
        double lowestPerformance;
        double highestPerformance;
        //network = webTrustCalculator.generateDirectTrust(network);
        int evaluatedId = -1;
        //Simulations
        List<Double> plotValues = new ArrayList<>();
        for (int i = 0; i < config.getIterationsQty(); i++) {
            switch ((int) config.getTypeOfEvolution()) {
                case 0:
                    lowestPerformance = 0;
                    highestPerformance = 0.5;
                    break;
                case 1:
                    lowestPerformance = -0.01;
                    highestPerformance = -0.5;
                    break;
                case 4:
                    if ((i % 2) == 0) {
                        lowestPerformance = 0;
                        highestPerformance = 0.5;
                    } else {
                        lowestPerformance = -0.01;
                        highestPerformance = -0.5;
                    }
                    break;
                case 2:
                    if ((RandGenerator.getRandDouble(false) % 2) == 0) {
                        lowestPerformance = 0;
                        highestPerformance = 0.5;
                    } else {
                        lowestPerformance = -0.01;
                        highestPerformance = -0.5;
                    }
                    break;
                default:
                    lowestPerformance = 0;
                    highestPerformance = 0.5;
                    break;
            }

            try {

                //Generate and quantfy web indirect Trus0.0.622             
                network = webTrustCalculator.quantifyTrustWebRankRelevance(network, this.config);
                List<Agent> sortedVertices = new ArrayList<>(network.getVertices());
                Collections.sort(sortedVertices);
                //Update trust for the lowest participant
                if (evaluatedId == -1) {
                    evaluatedId = sortedVertices.get(0).getId();
                }
                double performance = RandGenerator.getRandDouble(true, lowestPerformance, highestPerformance);
                //double performance = RandGenerator.getRandDouble(true, -0.25, 0);
                System.out.println("=================");
                for (Agent vertex : sortedVertices) {
                    System.out.println(vertex);
                    if (vertex.getId() == evaluatedId) {
                        //System.out.println(vertex);
                        plotValues.add(new Double(vertex.getRelevance()));
                    }
                }
                //TODO tiene un bug
                webTrustCalculator.updateTrust(network, performance, evaluatedId);

            } catch (Exception e) {
                break;
            }
        }
        if (config.isViewerEnabled()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
        }
    }

}
