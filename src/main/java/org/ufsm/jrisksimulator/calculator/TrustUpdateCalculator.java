/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.calculator;

import org.ufsm.jrisksimulator.models.SimulationConfig;

/**
 * Singleton class that implements trust update routine for isolation.
 * 
 * This class isolates the trust update procedure for RAD development, tests and isolation
 * 
 * @author tuxtor
 */
public class TrustUpdateCalculator {

    private TrustUpdateCalculator() {
    }

    /**
     * Singleton generator/access method
     * @return singleton TrustUpdateCalculator instance
     */
    public static TrustUpdateCalculator getInstance() {
        return TrustUpdateCalculatorHolder.INSTANCE;
    }

    /** 
     * Singleton holder
     */
    private static class TrustUpdateCalculatorHolder {

        private static final TrustUpdateCalculator INSTANCE = new TrustUpdateCalculator();
    }

    /**
     * Procedure that implements trust update computation.
     * This procedure implements trust update computation based on TrustWebRank
     * update function modification
     * @param directTrust direct trust score
     * @param performance performance score
     * @param paramValues simulation config
     * @return new trust value computed with the update function
     */
    public static double updateTrust(double directTrust, double performance, SimulationConfig paramValues) {
        double newTrust;
        double gamma = paramValues.getGammaQty();
        double tetha = paramValues.getTethaQty();
        //if ((performance > uthr)||((-uthr<=performance)&&(performance<=0))) {
        if ((performance >= 0)) {
            newTrust = directTrust + (1 - gamma) * Math.abs(performance);
        } else {
            newTrust = directTrust - (1 - tetha) * Math.abs(performance);
        }
        //New trust logical limit
        if (newTrust < 0) {
            return 0;
        } else {
            return newTrust;
        }
    }
}
