/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.calculator;

import edu.uci.ics.jung.graph.Graph;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.summary.Sum;
import org.apache.commons.math3.util.Precision;
import org.ufsm.jrisksimulator.models.Agent;
import org.ufsm.jrisksimulator.models.PeerScore;
import org.ufsm.jrisksimulator.models.PerformanceProfile;
import org.ufsm.jrisksimulator.models.TrustProfile;
import org.ufsm.jrisksimulator.util.ScoreGenerator;
import org.ufsm.jrisksimulator.models.SimulationConfig;
import org.ufsm.jrisksimulator.util.RandGenerator;

/**
 * This class is in charge of the computation of/for trust scores.
 * 
 * The utility routines implemented in this class aim to reproduce trust generation,
 * quantification, and analysis for the dissertation proposal.
 * @author tuxtor
 */
public class WebTrustCalculator {

    private SimulationConfig paramValues;

    /**
     * Singleton generator/access method
     * @return singleton WebTrustCalculator instance
     */
    public static WebTrustCalculator getInstance() {
        return WebCalculatorHolder.INSTANCE;
    }

    /** 
     * Singleton holder
     */
    private static class WebCalculatorHolder {

        private static final WebTrustCalculator INSTANCE = new WebTrustCalculator();
    }

    /**
     * Quantifies the impact of the relevance in the CRI computation.
     * In this method the direct trust and the relevance are generated according
     * to the simulation profiles to evaluate the full potential of the trust
     * inside the CRI formula.
     * @param network Social Network to be analyzed
     * @param paramValues simulation config
     * @return Social Network with computed relevance values
     */
    public Graph quantifyControlledRelevance(Graph network, SimulationConfig paramValues) {
        this.setSimulationConfig(paramValues);
        //Generate direct trust
        network = generateDirectTrust(network);
        //Quantify indirect trust
        network = quantifyProfileRelevance(network);

        return network;
    }

    /**
     * Quantifies the relevance using TrustWebRank implementation strictly.
     * In this method the direct trust is generated and the relevance is computed
     * with TrustWebRank to evaluate the full evolution of TrustWebRank trust coefficients.
     * @param network Social Network to be analyzed
     * @param paramValues simulation config
     * @return Social Network with computed relevance values
     */
    public Graph quantifyTrustWebRankRelevance(Graph network, SimulationConfig paramValues) {
        this.setSimulationConfig(paramValues);
        //Generate direct trust
        if (paramValues.isBootstrapTrustEnabled()) {
            network = generateDirectTrust(network);
        } else {
            network = generateFixedDirectTrust(network, paramValues.getFixedPeerTrust());
        }
        //Quantify indirect trust
        network = quantifyRelevance(network);
        return network;
    }

    /**
     * Generates trust scores between the members of the social network.
     * This procedure traverses each of the social network nodes generating 
     * direct trust scores according to trust profiles and simulation configuration
     * @param network Social Network to be populated with trust scores
     * @return Social Network with generated trust scores
     */
    private Graph generateDirectTrust(Graph network) {
        ScoreGenerator scoreGenerator = new ScoreGenerator(0, 1);
        int companion = (int) (getSimulationConfig().getEvaluatorsQty() - getSimulationConfig().getGoodOpinionsQty());
        int friends = (int) getSimulationConfig().getGoodOpinionsQty();
        //Set trust profile
        Iterator verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            if (friends-- > 0) {
                vertex.setProfile(TrustProfile.FRIEND);
            } else if (companion-- > 0) {
                vertex.setProfile(TrustProfile.COMPANION);
            } else {
                vertex.setProfile(TrustProfile.KNOWN);
            }
            //System.out.println(vertex.getId() + "-" + vertex.getProfile());
        }
        //Traverse graph node by node and create DIRECT risk score
        verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {

            Agent vertex = (Agent) verticesIterator.next();
            vertex.getPeersScore().clear();
            Iterator peersIterator = network.getNeighbors(vertex).iterator();
            while (peersIterator.hasNext()) {
                Agent peer = (Agent) peersIterator.next();
                //Qualifying
                vertex.getPeersScore().add(new PeerScore(peer.getId(), scoreGenerator.generateScore(true, peer.getProfile())));
            }
        }
        return network;
    }

    /**
     * Sets trust scores between the members of the social network.
     * This procedure traverses each of the social network nodes setting
     * a fixed direct trust score
     * @param network Social Network to be populated with trust scores
     * @return Social Network with preconfigured direct trust global score
     */
    private Graph generateFixedDirectTrust(Graph network, double fixedDirectTrustValue) {
        //Set trust profile
        Iterator verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            vertex.getPeersScore().clear();
            Iterator peersIterator = network.getNeighbors(vertex).iterator();
            while (peersIterator.hasNext()) {
                Agent peer = (Agent) peersIterator.next();
                //Qualifying
                vertex.getPeersScore().add(new PeerScore(peer.getId(), fixedDirectTrustValue));
            }
        }
        return network;
    }

    /**
     * Generates a direct relevance score based on trust profiles.
     * 
     * To simplify and make evident the impact of relevance this method generates
     * the relevance scores bases on the trust profiles of the risk committee
     * participants.
     *
     * @param network Social Network to be populated with relevance scores
     * @return Social Network with generated relevance scores
     */
    private Graph quantifyProfileRelevance(Graph network) {
        ScoreGenerator scoreGenerator = new ScoreGenerator(0, 1);
        Iterator verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            vertex.setRelevance(scoreGenerator.generateScore(true, vertex.getProfile()));
        }
        return network;
    }

//<editor-fold defaultstate="collapsed" desc="TrustWebRank methods">
    /**
     * Computes relevance scores by implementing TrustWebRank.
     * 
     * This method quantifies the relevance scores based on the direct trust scores
     * and the matrix version of TrustWebRank. All matrix and math operations 
     * were implemented using Apache Commons routines.
     *
     * @param network Social Network to be populated with relevance scores
     * @return Social Network with computed relevance scores
     */
    public Graph quantifyRelevance(Graph network) {
        double beta = this.getSimulationConfig().getBethaQty();
        System.out.println("Betha ="+beta);
        double[][] trustMatrix = createTrustMatrix(network);
        RealMatrix realTrustMatrix = MatrixUtils.createRealMatrix(trustMatrix);
        //System.out.println("Real =======================");

        double[][] stochasticMatrix = createStochasticMatrix(trustMatrix);
        RealMatrix realStochasticMatrix = MatrixUtils.createRealMatrix(stochasticMatrix);
        //System.out.println("Sto =======================");

        //RealMatrix realIndirectTrustMatrix = realStochasticMatrix.add(realStochasticMatrix.scalarMultiply(beta).multiply(realTrustMatrix))   ;
        RealMatrix identityMatrix = MatrixUtils.createRealIdentityMatrix(network.getVertexCount());
        RealMatrix multiplication = realStochasticMatrix.scalarMultiply(beta);
        RealMatrix substraction = identityMatrix.subtract(multiplication);
        RealMatrix inverse = new LUDecomposition(substraction).getSolver().getInverse();

        RealMatrix realIndirectMatrix = inverse.multiply(realStochasticMatrix);
        //MatrixUtils.createRealIdentityMatrix(network.getVertexCount());
        //(realIndirectMatrix.getData());
        //System.out.println("Indirect=======================");
        RealMatrix realIndirectStochasticMatrix = MatrixUtils.createRealMatrix(createStochasticMatrix(realIndirectMatrix.getData()));
        //System.out.println("Indirect stochastic=======================");

        List<Double> importance = calculateRelevance(realIndirectMatrix.getData());
        //ScoreGenerator scoreGenerator = new ScoreGenerator(0, 1);
        Iterator verticesIterator = network.getVertices().iterator();
        int index = 0;
        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            double globalTrust = importance.get(index++);
            vertex.setRelevance(globalTrust);
        }
        return network;
    }

    /**
     * Creates trust matrix for TrustWebRank computation.
     * @param network Social Network to be computed
     * @return matrix representation of the trust individual scores
     */
    private double[][] createTrustMatrix(Graph network) {
        int size = network.getVertexCount();
        double[][] trustMatrix = new double[size][size];

        List<Agent> pivotAgentList = new ArrayList(network.getVertices());

        Iterator networkIterator = network.getVertices().iterator();
        int i = 0;
        while (networkIterator.hasNext()) {
            Agent vertex = (Agent) networkIterator.next();
            int j = 0;
            for (Agent pivotAgent : pivotAgentList) {
                //Search on pears or asign 0
                double pivotScore = 0;
                for (PeerScore peerScore : vertex.getPeersScore()) {
                    if (peerScore.getPeerId() == pivotAgent.getId()) {
                        pivotScore = peerScore.getDirectTrustScore();
                        break;
                    }
                }
                trustMatrix[i][j] = pivotScore;
                j++;
            }
            i++;
        }
        //printAgentsList(network);
        //printMatrix(trustMatrix);
        return trustMatrix;
    }

    /**
     * Creates stochastic matrix for TrustWebRank computation.
     * 
     * As described in TrustWebRank description it is necessary to use normalized
     * trust values and this is achieved by creating a stocastic matrix
     * 
     * @param trustMatrix matrix representation of trust scores in the social networks
     * @return matrix representation of normalized trust scores
     */
    private double[][] createStochasticMatrix(double[][] trustMatrix) {
        RealMatrix trustRealMatrix = MatrixUtils.createRealMatrix(trustMatrix);
        double[][] stochasticMatix = new double[trustMatrix.length][trustMatrix.length];

        for (int i = 0; i < trustMatrix.length; i++) {
            double[] neighborhood = trustRealMatrix.getRow(i);
            Sum neighborhoodSum = new Sum();
            for (int k = 0; k < neighborhood.length; k++) {
                neighborhoodSum.increment(neighborhood[k]);
            }
            for (int j = 0; j < trustMatrix.length; j++) {
                double directTrust = trustMatrix[i][j];
                stochasticMatix[i][j] = directTrust / neighborhoodSum.getResult();
            }

        }
        //printMatrix(stochasticMatix);
        return stochasticMatix;
    }
   
    /**
     * Computes TrustWebRank relevance scores.
     * 
     * Computes relevance based on dissertations proposal.
     * 
     * @param trustMatrix matrix representation of trust scores in the social networks
     * @return list of relevance scores for the input matrix
     */
    private List<Double> calculateRelevance(double[][] indirectTrustMatrix) {

        List<Double> importanceList = new ArrayList<>();
        double tau = 0.01;

        RealMatrix realIndirectTrustMatrix = MatrixUtils.createRealMatrix(indirectTrustMatrix);
        for (int i = 0; i < indirectTrustMatrix.length; i++) {
            DescriptiveStatistics trustersSum = new DescriptiveStatistics();
            double[] trusters = realIndirectTrustMatrix.getColumn(i);
            for (int k = 0; k < trusters.length; k++) {
                if (trusters[k] > tau) {
                    trustersSum.addValue(trusters[k]);
                }
            }
            double importance = trustersSum.getMean();
            if (importance > 1) {
                importanceList.add((double) 1);
            } else if (Double.isNaN(importance)) {
                importanceList.add((double) 0);
            } else {
                importanceList.add(importance);
            }

        }
        return importanceList;
    }

    /**
     * Updates individual risk committee trust simulating performance.
     * Since the update of trust is mandatory on each cycle, this method simulates
     * gain or loss performance and updates the direct trust between the committee
     * members and their neighbors. 
     * 
     * @param network original social network to be updated
     * @param riskCommittee list of risk assessment committee members
     * @param config simulation config
     * @return social network with updated direct trust scores
     */
    public Graph updateTrust(Graph network, List<Agent> riskCommittee, SimulationConfig config) {
        ScoreGenerator generator = new ScoreGenerator();
        double positivePerformance;
        double negativePerformance;
//        if (config.isFixedPerformanceEnabled()) {
//            positivePerformance = config.getFixedPerformance();
//            negativePerformance = config.getFixedPerformance()*-1;
//        } else {
        positivePerformance = generator.generateScore(true, PerformanceProfile.GAIN);
        negativePerformance = generator.generateScore(true, PerformanceProfile.LOSS);

        //}
        //public double negativePerformance = 
        //Traverse graph node by node and update DIRECT risk score
        Iterator verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            Iterator neighborsIterator = network.getNeighbors(vertex).iterator();
            while (neighborsIterator.hasNext()) {
                Agent peer = (Agent) neighborsIterator.next();
                if (!riskCommittee.contains(peer)) {
                    continue;
                }
                //Search peer for update
                for (PeerScore peerScore : vertex.getPeersScore()) {
                    if (peerScore.getPeerId() == peer.getId()) {
                        double oldTrustScore = peerScore.getDirectTrustScore();
                        double newTrustScore;
                        if (config.isFixedPerformanceEnabled()) {
                            if (peer.getPerformanceProfile() == PerformanceProfile.GAIN) {
                                newTrustScore = TrustUpdateCalculator.updateTrust(oldTrustScore, positivePerformance, paramValues);
                            } else {
                                newTrustScore = TrustUpdateCalculator.updateTrust(oldTrustScore, negativePerformance, paramValues);
                            }
                        } else {
                            double rand = RandGenerator.getRandDouble(false, 0, 10);
                            newTrustScore = TrustUpdateCalculator.updateTrust(oldTrustScore, rand < 6 ? negativePerformance : positivePerformance, paramValues);
                        }
                        peerScore.setDirectTrustScore(newTrustScore);
                        break;
                    }
                }
            }
        }
        network = quantifyRelevance(network);
        return network;
    }

    /**
     * Trust update method for coefficients simulation.
     * This method updates trust scores based only on fixed performance to avoid
     * any interference to the coefficient simulations.
     * 
     * @param network original social network to be updated
     * @param riskCommittee list of risk assessment committee members
     * @param config simulation config
     * @return social network with updated direct trust scores
     */
    public Graph updateTrustForCoeficients(Graph network, List<Agent> riskCommittee, SimulationConfig config) {
        ScoreGenerator generator = new ScoreGenerator();        
        Iterator verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            Iterator neighborsIterator = network.getNeighbors(vertex).iterator();
            while (neighborsIterator.hasNext()) {
                Agent peer = (Agent) neighborsIterator.next();
                if (!riskCommittee.contains(peer)) {
                    continue;
                }
                //Search peer for update
                for (PeerScore peerScore : vertex.getPeersScore()) {
                    if (peerScore.getPeerId() == peer.getId()) {
                        double oldTrustScore = peerScore.getDirectTrustScore();
                        double newTrustScore;
                        if(config.getTypeOfEvolution()==0){
                            newTrustScore = TrustUpdateCalculator.updateTrust(oldTrustScore, config.getFixedPerformance(), paramValues);
                        }else{
                            newTrustScore = TrustUpdateCalculator.updateTrust(oldTrustScore, config.getFixedPerformance()*-1, paramValues);
                        }
                        
                        peerScore.setDirectTrustScore(newTrustScore);
                        break;
                    }
                }
            }
        }
        network = quantifyRelevance(network);
        return network;
    }
    
    /**
     * Prints the list of agent contained in a given social network.
     * @param network network to be printed.
     */
    public void printAgentsList(Graph network) {
        Iterator networkIterator = network.getVertices().iterator();
        String str = "|\t";
        while (networkIterator.hasNext()) {
            Agent vertex = (Agent) networkIterator.next();
            str += vertex.getId() + "\t";
        }
        System.out.println(str + "|");
    }

    /**
     * Prints an array of arrays in standard output.
     * @param m matrix to be printed
     */
    public void printMatrix(double[][] m) {
        try {
            int rows = m.length;
            int columns = m[0].length;
            String str = "|\t";

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    str += Precision.round(m[i][j], 4) + "\t";
                }

                System.out.println(str + "|");
                str = "|\t";
            }

        } catch (Exception e) {
            System.out.println("Matrix is empty!!");
        }
    }
    
    /**
     * Deprecated trust update method for {@Link MainWindowController#simulateEvolution}
     * @param network
     * @param performance
     * @param id
     * @return
     * @deprecated
     */
    @Deprecated
    public Graph updateTrust(Graph network, double performance, int id) {
        //Traverse graph node by node and create DIRECT risk score
        Iterator verticesIterator = network.getVertices().iterator();
        while (verticesIterator.hasNext()) {

            Agent vertex = (Agent) verticesIterator.next();
            for (PeerScore score : vertex.getPeersScore()) {
                if (score.getPeerId() == id) {
                    score.setDirectTrustScore(TrustUpdateCalculator.updateTrust(score.getDirectTrustScore(), performance, paramValues));
                }
            }
        }
        return network;
    }
//</editor-fold>

    public SimulationConfig getSimulationConfig() {
        return paramValues;
    }

    public void setSimulationConfig(SimulationConfig paramValues) {
        this.paramValues = paramValues;
    }
    
    
}
