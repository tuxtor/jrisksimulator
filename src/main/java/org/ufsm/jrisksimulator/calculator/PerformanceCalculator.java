/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.calculator;

import java.util.ArrayList;
import java.util.List;
import org.ufsm.jrisksimulator.models.RiskCRI;
import org.ufsm.jrisksimulator.models.RiskEventKRI;
import org.ufsm.jrisksimulator.util.ScoreGenerator;

/**
 * Mockup class aimed to simulate performance.
 * 
 * This singleton generates KRI performance scores in order to give feedback for
 * SNA algorithms.
 * @author tuxtor
 */
public class PerformanceCalculator {

    private int perRiskKRIQty = 5;

    /**
     * Singleton generator/access method
     * @return singleton PerformanceCalculator instance
     */
    public static PerformanceCalculator getInstance() {
        return PerformanceCalculatorHolder.INSTANCE;
    }

    /** 
     * Singleton holder
     */
    private static class PerformanceCalculatorHolder {

        private static final PerformanceCalculator INSTANCE = new PerformanceCalculator();
    }

    /**
     * Creates a performance risk indicator based on generated KRIs profiles.
     * @param globalRiskScores calculated CRI per risk
     * @param perRiskKRIQty quantity of KRI per Risk
     * @return List of CRI with their correspondent PRisk
     */
    public List<RiskCRI> quantifyPerformance(List<RiskCRI> globalRiskScores, int perRiskKRIQty) {
        ScoreGenerator generator = new ScoreGenerator(0, 1);
        //TODO this is a stub for KRI quantification based on risk performance profiles, it needs to be implemented
        for(RiskCRI riskCri:globalRiskScores){
            riskCri.setRiskPerformance(generator.generateScore(true, riskCri.getPerformanceProfile()));
        }
        return globalRiskScores;
    }

}
