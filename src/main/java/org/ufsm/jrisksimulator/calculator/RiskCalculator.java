/* 
 * Copyright 2014 Víctor Orozco.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ufsm.jrisksimulator.calculator;

import edu.uci.ics.jung.graph.Graph;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.ufsm.jrisksimulator.models.Agent;
import org.ufsm.jrisksimulator.models.LikertScore;
import org.ufsm.jrisksimulator.models.RiskCRI;
import org.ufsm.jrisksimulator.models.PerformanceProfile;
import org.ufsm.jrisksimulator.models.RiskScore;
import org.ufsm.jrisksimulator.models.TrustProfile;
import org.ufsm.jrisksimulator.util.ScoreGenerator;
import org.ufsm.jrisksimulator.models.SimulationConfig;

/**
 * This class is in charge of the computation of/for risk scores.
 * 
 * The utility routines implemented in this class aim to reproduce risk behavior,
 * quantification, and analysis for the dissertation proposal.
 * @author tuxtor
 */
public class RiskCalculator {

    private int riskAvaliatorsQty;
    private SimulationConfig paramValues;

    private RiskCalculator() {
    }

    /**
     * Singleton generator/access method
     * @return singleton WebTrustCalculator instance
     */
    public static RiskCalculator getInstance() {
        return RiskCalculatorHolder.INSTANCE;
    }

    private static class RiskCalculatorHolder {

        private static final RiskCalculator INSTANCE = new RiskCalculator();
    }

    public List<RiskCRI> quantifyRisks(Graph network, SimulationConfig paramValues) {
        this.paramValues = paramValues;
        //Create the risk assessment commitee using the high trustable peers
        List<Agent> sortedVertices = new ArrayList<>(network.getVertices());
        Collections.sort(sortedVertices);
        //Selects the most trustable agents
        List<Agent> highVertices = sortedVertices.subList(0, (int) paramValues.getEvaluatorsQty());
        //Generate the trust opinions using the highest trusted peers
        highVertices = generateRiskScores(highVertices, (int) paramValues.getRisksQty(), paramValues.isTrustEnabled());
        return quantifyRiskScores(highVertices);
    }

    /**
     * Generates the risk opinions of every agent that belongs to the risk
     * assessment committee.
     * 
     * This procedure generates risk assessment opinions for every risk and every
     * member of the risk assessment committee based on his trust profile.
     *
     * @param highVertices list of agents whose opinions will be generated
     * @param risksQty quantity of risks and consequently opinions to be generated
     * @return list of risk assessment participants with their correspondent
     * trust opinions
     */
    private List<Agent> generateRiskScores(List<Agent> highVertices, int risksQty, boolean trustEnabled) {
        ScoreGenerator scoreGenerator = new ScoreGenerator(0, 1);

        LikertScore lowRiskScore;
        LikertScore upRiskScore;

        for (Agent vertex : highVertices) {
            vertex.getRisksScore().clear();
            int riskId = 0;
            while (risksQty > riskId) {
                riskId++;
                if ((vertex.getProfile() == TrustProfile.FRIEND) && (riskId < 4)) {
                    lowRiskScore = LikertScore.MEDIUM  ;
                    upRiskScore = LikertScore.HIGH;
                } else if ((vertex.getProfile() == TrustProfile.COMPANION) && (riskId < 4)) {
                    lowRiskScore = LikertScore.VERY_LOW;
                    upRiskScore = LikertScore.MEDIUM;
                } else {
                    lowRiskScore = LikertScore.VERY_LOW;
                    upRiskScore = LikertScore.VERY_HIGH;
                }
                RiskScore riskScore;
                if (trustEnabled) {
                    riskScore = new RiskScore(riskId,
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            vertex.getRelevance());

                } else {
                    riskScore = new RiskScore(riskId,
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore),
                            scoreGenerator.generateLikertScore(lowRiskScore, upRiskScore));
                }
                vertex.getRisksScore().add(riskScore);
            }
        }
        return highVertices;
    }

    /**
     * Joins risks opinions creating a list of Risk CRIs.
     * This procedure computes CRI for each of the risks based on Agent generated
     * risk assessment opinions.
     * @param highVertices list of the risk assessment committee members
     * @return list of risk CRIs
     */
    private List<RiskCRI> quantifyRiskScores(List<Agent> highVertices) {

        Map<Integer, RiskCRI> globalRisksMap = new TreeMap<>();
        //Colapse per risk qualifications
        for (Agent agent : highVertices) {
            for (RiskScore riskScore : agent.getRisksScore()) {
                RiskCRI riskCRI;
                if (globalRisksMap.containsKey(riskScore.getId())) {
                    riskCRI = globalRisksMap.get(riskScore.getId());
                } else {
                    riskCRI = new RiskCRI(riskScore.getId());
                    riskCRI.setPerformanceProfile(PerformanceProfile.GAIN);//TODO todos ganan
                }
                riskCRI.updateCRI(riskScore.getAverage());
                globalRisksMap.put(riskCRI.getId(), riskCRI);
            }
        }
        //Convert to global list
        return new ArrayList<>(globalRisksMap.values());

    }

    /**
     * Legacy risk score generation, conserved for reference purposes
     */
    @Deprecated
    private Graph generateRiskScores(Graph network, int risksQty) {
        ScoreGenerator scoreGenerator = new ScoreGenerator(0, 1);
        Iterator verticesIterator = network.getVertices().iterator();

        while (verticesIterator.hasNext()) {
            Agent vertex = (Agent) verticesIterator.next();
            int riskId = 0;
            while (risksQty > riskId) {
                riskId++;
//                RiskScore riskScore = new RiskScore(riskId,
//                        scoreGenerator.generateScore(true),
//                        scoreGenerator.generateScore(true),
//                        scoreGenerator.generateScore(true),
//                        scoreGenerator.generateScore(true),
//                        scoreGenerator.generateScore(true));
            }
        }
        return network;
    }
}
