JRiskSimulator
==============

JRiskSimulator é um simulador personalizado visado para a realização de testes de analise de redes sociais e gestão de riscos de segurança de informação. Entre os objetivos da sua criação esta a personalização das condições de teste sem influencia direta nos resultados, evitando qualquer tipo de influência na forma de convergência para resultados ideais mas não realistas. 

Para atingir o seu objetivo JRiskSimulator foi construído utilizando rotinas de geração de valores aleatórios para aqueles valores que normalmente seriam coletados durante o ciclo de gestão de riscos. Com a criação das rotinas de geração, o usuário estabelece as condições de simulação através de perfis que representam intervalos predeterminados de valores, permitindo assim a introdução de um elemento de aleatoriedade onde o usuário tem a opção de selecionar perfis de comportamento para a geração de valores e a opção de estabelecer valores para os coeficientes de controle, sem a possibilidade de influenciar diretamente nos valores finais que serão utilizados dentro das formulas de estimativas de riscos e avaliação de desempenho. 

JRiskSimulator esta construido com base em 4 grandes modulos sendo estes:

![Arquitetura](../images/simulador.png "Arquitetura JRiskSimulator")


A equivalencia destes modulos com o codigo Java é a seguinte:

* Simulador de avaliações - [org.ufsm.jrisksimulator.controllers.MainWindowController](apidocs/org/ufsm/jrisksimulator/controllers/MainWindowController.html)
* Gerenciador de redes sociales - classe [org.ufsm.jrisksimulator.graph.GraphFabric](apidocs/org/ufsm/jrisksimulator/graph/GraphFabric.html)
* Avaliadores de confiança - classe [org.ufsm.jrisksimulator.calculator.WebTrustCalculator](apidocs/org/ufsm/jrisksimulator/calculator/WebTrustCalculator.html)
* IndicadoresKri - classe [org.ufsm.jrisksimulator.calculator.PerformanceCalculator](apidocs/org/ufsm/jrisksimulator/calculator/PerformanceCalculator.html)
* Estimativas de risco - classe ´[org.ufsm.jrisksimulator.calculator.RiskCalculator](apidocs/org/ufsm/jrisksimulator/calculator/RiskCalculator.html)

A interação destas classes é detalhada dentro da documentação [Javadoc](apidocs/) do projeto.

Liks rapidos:

* [Lista de ferramentas a ser utilizadas para o desenvolvimento do projeto](ferramentas.html)
* [Repositorio em BitBucket contendo o codigo fonte do projeto](https://bitbucket.org/tuxtor/jrisksimulator/)