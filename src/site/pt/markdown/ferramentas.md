Ferramentas
===========

Para a utilização, atualização e compilação do projeto é preciso de um entorno com pelo menos os componentes seguintes:

* [Java Developer Kit][0] nível de código 1.7 com suporte para JavaFX (testado com jdk-7u60 em Gentoo Linux).
* Um IDE com suporte para gerenciamento de projetos com Maven (o projeto foi testado com [Netbeans 8][1]).
* [JavaFX Scene Builder 2.0][2] para a edição gráfica da interface grafica (suportado em Netbeans 8).
* [Apache Maven 3.x (testado com 3.2.1)][3]. Caso Netbeans seja utilizado o mesmo inclui suporte para Maven é não é necessaria a instalação independente no sistema operativo.

As dependências(bibliotecas) do projeto foram especificadas no arquivo de configuração Maven pom.xml, as mesmas estão disponíveis em Maven Central e podem ser instaladas via Netbeans e/ou seram descargadas na primeira compilação via Maven.

[0]: http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
[1]: https://netbeans.org/
[2]: http://www.oracle.com/technetwork/java/javase/overview/javafx-overview-2158620.html
[3]: http://maven.apache.org/download.cgi

